'use strict';

angular.module('myApp.viewPost', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider
    .when('/post', {
      templateUrl: 'viewPost/viewPost.html',
      controller: 'viewPostCtrl'
    });
}])

.factory('Post', ['$resource',
    function($resource) {
      return $resource('/api/post', {
        postId: 'postId'
      }, {
        query: {
          method: 'GET',
          params: {
            postId: ''
          },
          isArray: true
        },
        create: {
          method: 'POST',
          params: {
            postId: ''
          },
          isArray: false
        },
        delete: {
          method: 'DELETE',
          params: {
            postId: ''
          },
          isArray: false
        },
      });
    }
  ])
  .controller('viewPostCtrl', ['$scope', '$log', 'Post',
    function($scope, $log, Post) {
      $scope.posts = [];
      $scope.posts = Post.query();
      $scope.asin = '';
      $scope.delete = function(post) {
        Post.delete({
          _id: post._id
        }, function(res) {
          if (res.status === 'ok') {
            for (var i = $scope.posts.length - 1; i >= 0; i--) {
              if ($scope.posts[i]._id === post._id) {
                $scope.posts.splice(i, 1);
                break;
              };

            };
          };
        });
      };

      $scope.createpost = function() {
        $log.log('yoyo');
        Post.create({asin: $scope.asin}, function(res) {
          $log.log(res);
        });
      };
    }
  ]);
