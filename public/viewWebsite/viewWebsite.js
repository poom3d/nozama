'use strict';

angular.module('myApp.viewWebsite', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider
    .when('/website', {
      templateUrl: 'viewWebsite/viewWebsite.html',
      controller: 'viewWebsiteCtrl'
    })
    .when('/websiteDetail', {
      templateUrl: 'viewWebsite/viewWebsiteDetail.html',
      controller: 'viewWebsiteDetailCtrl'
    })
    .when('/website/:websiteId', {
      templateUrl: 'viewWebsite/viewWebsiteDetail.html',
      controller: 'viewWebsiteDetailCtrl'
    });
}])

.factory('Website', ['$resource',
    function($resource) {
      return $resource('/api/website/:websiteId', {
        websiteId: 'websiteId'
      }, {
        query: {
          method: 'GET',
          params: {
            websiteId: ''
          },
          isArray: true
        },
        get: {
          method: 'GET',
          params: {
            websiteId: ''
          },
          isArray: false
        },
        put: {
          method: 'PUT',
          params: {
            websiteId: ''
          },
          isArray: false
        },
        post: {
          method: 'POST',
          params: {
            websiteId: ''
          },
          isArray: false
        },
        delete: {
          method: 'DELETE',
          params: {
            websiteId: ''
          },
          isArray: false
        },
      });
    }
  ])
  .controller('viewWebsiteCtrl', ['$scope', '$log', 'Website', function($scope,
    $log, Website) {
    $scope.websites = [];
    $scope.websites = Website.query();

    $scope.delete = function(website) {
      Website.delete({
        _id: website._id
      }, function(res) {
        if (res.status === 'ok') {
          for (var i = $scope.websites.length - 1; i >= 0; i--) {
            if ($scope.websites[i]._id === website._id) {
              $scope.websites.splice(i, 1);
              break;
            };

          };
        };
      });
    };
  }])
  .controller('viewWebsiteDetailCtrl', ['$scope', '$log', '$routeParams',
    '$location', 'Website', 'IconService',
    function($scope, $log, $routeParams, $location, Website, IconService) {

      $scope.warning = false;
      $scope.upgrade = false;
      $scope.icons = IconService.query();
      if ($routeParams.websiteId !== undefined) { // Update
        $scope.website = Website.get({
          websiteId: $routeParams.websiteId
        }, function (website) {
           $scope.logo = website.logo;
        });
      } else { // Insert
        $scope.website = {
          url: ''
        };
      };

      $scope.save = function() {
        if ($scope.website._id !== undefined) {
          if ($scope.logo) {
            $scope.website.logo = $scope.logo._id;
          } else {
            $scope.website.logo = null;
          };

          $log.log($scope.website);
          Website.put($scope.website, function(res) {
            if (res && res.status === 'ok') {
              $scope.success = true;
              $scope.warning = false;
            } else if (res.status === 'limit') {
              $scope.warning = true;
              $scope.upgrade = true;
              $scope.success = false;
              $scope.message = '10 websites limit exceeded.';
            } else {
              $log.log(res);
              $scope.warning = true;
              $scope.success = false;
              $scope.message = res.errorMsg;
            };
          });
        } else {
          if ($scope.logo) {
            $scope.website.logo = $scope.logo._id;
          } else {
            $scope.website.logo = null;
          };
          Website.post($scope.website, function(res) {
            if (res && res.status === 'ok') {
              $location.path('/website');
              $scope.success = true;
              $scope.warning = false;
            } else if (res.status === 'limit') {
              $scope.warning = true;
              $scope.upgrade = true;
              $scope.success = false;
              $scope.message = '10 websites limit exceeded.';
            } else {
              $log.log(res);
              $scope.warning = true;
              $scope.success = false;
              $scope.message = res.errorMsg;
            };
          });
        };
      };
    }
  ]);
