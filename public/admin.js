'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
  'ngRoute',
  'ngResource',
  'myApp.viewKeyword',
  'myApp.viewKeywordDetail',
  'myApp.viewCategory',
  'myApp.viewWebsite',
  'myApp.viewIcon',
  'myApp.viewProfile',
  'myApp.viewNetwork',
  'myApp.viewPost',
  'myApp.viewPr',
  'myApp.version',
  'myApp.keyword',
  'myApp.icon',
  'myApp.category',
  'myApp.viewApp'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.otherwise({redirectTo: '/website'});
}]);
