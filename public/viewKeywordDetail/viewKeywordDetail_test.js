'use strict';

describe('myApp.viewKeyword module', function() {

  beforeEach(module('ngResource'));
  beforeEach(module('myApp.keyword'));
  beforeEach(module('myApp.viewKeywordDetail'));

  describe('viewKeyword controller', function(){

    var scope, ctrl;
    beforeEach(inject(function($rootScope, $controller) {
      scope = $rootScope.$new();
      ctrl = $controller('viewKeywordDetailCtrl', {$scope: scope});
    }));

    it('should test viewKeyword', (function() {
      //spec body
      var viewKeywordDetailCtrl = ctrl;
      expect(scope.keyword).toBeDefined();
      expect(viewKeywordDetailCtrl).toBeDefined();
    }));

  });
});