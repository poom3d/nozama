'use strict';

angular.module('myApp.viewKeywordDetail', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/keyword/:keywordId', {
      templateUrl: 'viewKeywordDetail/viewKeywordDetail.html',
      controller: 'viewKeywordDetailCtrl'
    })
    .when('/keywordDetail', {
      templateUrl: 'viewKeywordDetail/viewKeywordDetail.html',
      controller: 'viewKeywordDetailCtrl'
    });
}])
.factory('Review', ['$resource',
function($resource){
  return $resource('/api/review/:asin', {asin: 'asin'}, {
    get: {method:'GET', params:{asin:''}, isArray:false},
  });
}])
.controller('viewKeywordDetailCtrl', ['$scope', '$log', '$routeParams',
  '$location', 'Keyword', 'Website', 'Review',
  function($scope, $log, $routeParams, $location, Keyword, Website, Review) {
    $scope.keyword = {};
    $scope.websites = [];

    // I suspect a race condition bug. Quick fix.
    $scope.websitesReady = false;
    $scope.keywordReady = false;

    $scope.websites = Website.query(function (res) {
      $scope.websitesReady = true;
      $scope.setWebsite();
    });

    if ($routeParams.keywordId !== undefined && typeof($routeParams.keywordId) !==
      'undefined') {
      $scope.keyword = Keyword.get({
          keywordId: $routeParams.keywordId
        },
        function(keyword) {
          // Set website
          $scope.keyword = keyword;
          $scope.keywordReady = true;
          $scope.setWebsite();
        });
    };

    $scope.setWebsite = function() {
      if ($scope.websitesReady && $scope.keywordReady) {
        // Set the website
        for (var i = $scope.websites.length - 1; i >= 0; i--) {
          $log.log($scope.websites[i]);
          if ($scope.keyword.website && $scope.websites[i]._id.toString() === $scope.keyword.website.toString()) {
            $scope.website = $scope.websites[i];
          };
        };
      };
    }


    $scope.save = function() {
      if ($scope.keyword._id !== undefined) {
        Keyword.put($scope.keyword, function(res) {
          //$location.path('/keyword');
          $scope.success = true;
        });
      } else {

        $log.log($scope.keyword);
        $log.log($scope.website);

        if ($scope.website === undefined) {
          alert('Please select website.');
        } else {

          // Set website Id
          $scope.keyword.website = $scope.website._id;

          Keyword.post($scope.keyword, function(res) {
            $log.log(res);
            $scope.success = true;
            $location.path('/keyword');
          });
        };

      };

    };

    $scope.getReview = function() {

      if ($scope.keyword.asin && $scope.keyword.asin !== '') {
        $log.log($scope.keyword.asin);
        Review.get({asin: $scope.keyword.asin}, function (result) {
          $log.log(result);
          var index = Math.floor((Math.random() * result.reviews.length));
          $scope.keyword.content = result.reviews[index];
        });
      } else {
        alert('Please fill in ASIN');
      };
    };
  }
]);
