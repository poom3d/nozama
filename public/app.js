'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
  'ngRoute',
  'ngResource',
  'myApp.viewKeyword',
  'myApp.viewKeywordDetail',
  'myApp.viewCategory',
  'myApp.viewWebsite',
  'myApp.viewProfile',
  'myApp.viewUpgrade',
  'myApp.version',
  'myApp.keyword',
  'myApp.icon',
  'myApp.category'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.otherwise({redirectTo: '/website'});
}]);
