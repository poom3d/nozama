'use strict';

angular.module('myApp.viewUpgrade', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider
  .when('/upgrade', {
    templateUrl: 'viewUpgrade/viewUpgrade.html',
    controller: 'viewUpgradeCtrl'
  });
}])
.controller('viewUpgradeCtrl', ['$scope', '$log', 'Icon',
  function($scope, $log, Icon) {
  // Nothing in particular
}]);