'use strict';

angular.module('myApp.viewCategory', ['ngRoute'])

.config(['$routeProvider', function ($routeProvider) {
  $routeProvider.when('/research', {
    templateUrl: 'viewCategory/viewCategory.html',
    controller: 'viewCategoryCtrl'
  });
}])

.controller('viewCategoryCtrl', ['$scope', '$log', '$http', 'Category',
  'Subcategory', 'GenKeyword', 'Research', 'Website', 'Keyword', 'Profile',
  function viewCategory($scope, $log, $http, Category, Subcategory,
    GenKeyword, Research, Website, Keyword, Profile) {
    $scope.categories = [];
    $scope.subcategories = [];
    $scope.keywords = [];
    $scope.currentCategory = {};
    $scope.currentSubcategory = {};
    $scope.websites = [];
    $scope.premium = false;

    $scope.websites = Website.query();
    $scope.categories = Category.query();

    $scope.user = Profile.get(function (res) {
      if (res.package === 'premium' || res.status === 'admin') {
        $scope.premium = true;
      };
    });


    $scope.open = function (category) {
      $scope.currentCategory = category;
      $scope.currentSubcategory = {};
      $scope.subcategories = Subcategory.query({
        url: category.url
      });
    };

    $scope.openSubcategory = function (category) {
      $scope.currentSubcategory = category;
      //$scope.subcategories = Subcategory.query({url: category.url});
      Subcategory.query({
        url: category.url
      }, function (res) {
        if (res && res.length > 0) {
          $scope.subcategories = res;
        };
      });

      $scope.keywords = GenKeyword.query({
        url: category.url
      });

      // Get next page for premium
      if ($scope.premium) {
        $scope.page = GenKeyword.page({
          url: category.url
        });
      };

    };

    $scope.refreshKeyword = function (category) {
      $scope.keywords = GenKeyword.query({
        url: category.url
      });

      // Get next page for premium
      if ($scope.premium) {
        $scope.page = GenKeyword.page({
          url: category.url
        });
      };
    };

    $scope.research = function (keyword) {
      Research.get({
        keyword: keyword.keyword,
        _id: keyword._id
      }, function (res) {
        $log.log(res);
        keyword.googleResults = res.googleResults;
      });
    };

    $scope.refreshCache = function () {
      GenKeyword.delete({}, function (res) {
        console.log(res);
      });
    };

    $scope.saveKeyword = function (keyword) {
      // if ($scope.website !== undefined) {
      // $log.log('save!');
      if ($scope.website !== undefined) {
        keyword.website = $scope.website._id;
      };
      Keyword.post(keyword, function (res) {
        $log.log(res);
        keyword.saved = true;
      });
      // } else {
      //   alert('Please select website.');
      // };
    };

    $scope.nextPage = function () {
      $log.log($scope.page);

      if ($scope.page && $scope.page.next) {
        var url = $scope.page.next;
        $scope.openSubcategory({
          url: url
        });
      };
    };

    $scope.prevPage = function () {

      if ($scope.page && $scope.page.prev) {
        var url = $scope.page.prev;
        $scope.openSubcategory({
          url: url
        });
      };
    };

    $scope.gen = function () {

      if ($scope.currentSubcategory && $scope.currentSubcategory.url
        && $scope.website) {
        $http.post('/api/gen', {
          website: $scope.website,
          url: $scope.currentSubcategory.url
        }).
        success(function (data, status, headers, config) {
          // this callback will be called asynchronously
          // when the response is available
          $log.log(data);
        }).
        error(function (data, status, headers, config) {
          // called asynchronously if an error occurs
          // or server returns response with an error status.
          $log.log('error');
        });
      } else {
        $log.log('please select one');
        if (!$scope.website || $scope.website === '') {
          alert('Please Select Website');
        };
      };

    };
  }
]);
