'use strict';

describe('myApp.viewCategory module', function() {

  beforeEach(module('ngResource'));
  beforeEach(module('myApp.category'));
  beforeEach(module('myApp.viewCategory'));

  describe('viewCategory controller', function(){

    var scope, ctrl;
    beforeEach(inject(function($rootScope, $controller) {
      scope = $rootScope.$new();
      ctrl = $controller('viewCategoryCtrl', {$scope: scope});
    }));

    it('should test viewCategory', (function() {
      //spec body
      var viewCtrl = ctrl;
      expect(scope.categories).toBeDefined();
      expect(viewCtrl).toBeDefined();
    }));

  });
});