'use strict';

angular.module('myApp.viewIcon', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider
  .when('/icon', {
    templateUrl: 'viewIcon/viewIcon.html',
    controller: 'viewIconCtrl'
  })
  .when('/iconDetail', {
    templateUrl: 'viewIcon/viewIconDetail.html',
    controller: 'viewIconDetailCtrl'
  })
  .when('/icon/:iconId', {
    templateUrl: 'viewIcon/viewIconDetail.html',
    controller: 'viewIconDetailCtrl'
  });
}])

.factory('Icon', ['$resource',
function($resource){
  return $resource('/api/icon/:iconId', {iconId: 'iconId'}, {
    query: {method:'GET', params:{iconId:''}, isArray:true},
    get: {method:'GET', params:{iconId:''}, isArray:false},
    put: {method:'PUT', params:{iconId:''}, isArray:false},
    post: {method:'POST', params:{iconId:''}, isArray:false},
    delete: {method:'DELETE', params:{iconId:''}, isArray:false},
  });
}])
.controller('viewIconCtrl', ['$scope', '$log', 'Icon',
  function($scope, $log, Icon) {
  $scope.icons = [];
  $scope.icons = Icon.query();

  $scope.delete = function(icon) {
    Icon.delete({_id: icon._id}, function (res) {
      if (res.status === 'ok') {
        for (var i = $scope.icons.length - 1; i >= 0; i--) {
          if ($scope.icons[i]._id === icon._id) {
            $scope.icons.splice(i, 1);
            break;
          };

        };
      };
    });
  };
}])
.controller('viewIconDetailCtrl',
  ['$scope', '$log', '$routeParams', '$location', 'Icon',
  function($scope, $log, $routeParams, $location, Icon) {

  if ($routeParams.iconId !== undefined) {    // Update
    $scope.icon = Icon.get({iconId: $routeParams.iconId}, function (res) {
    });
  } else {                                      // Insert
    $scope.icon = {url: ''};
  };

  $scope.save = function() {
    if ($scope.icon._id !== undefined) {
      $log.log($scope.icon);
      Icon.put($scope.icon, function(res) {
        if (res && res.status === 'ok') {
        };
      });
    } else {
      Icon.post($scope.icon, function(res) {
        if (res && res.status === 'ok') {
          $location.path('/icon');
        };
      });
    };
  };
}]);