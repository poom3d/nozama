'use strict';

angular.module('myApp.keyword', [])
.factory('Keyword', ['$resource',
function($resource){
  return $resource('/api/keyword/:keywordId', {keywordId: 'keywordId'}, {
    query: {method:'GET', params:{keywordId:''}, isArray:true},
    get: {method:'GET', params:{keywordId:''}, isArray:false},
    put: {method:'PUT', params:{keywordId:''}, isArray:false},
    post: {method:'POST', params:{keywordId:''}, isArray:false},
    delete: {method:'DELETE', params:{keywordId:''}, isArray:false}
  });
}]);
