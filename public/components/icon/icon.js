'use strict';

angular.module('myApp.icon', [])
.factory('IconService', ['$resource',
function($resource){
  return $resource('/api/icon/:iconId', {iconId: 'iconId'}, {
    query: {method:'GET', params:{iconId:''}, isArray:true},
    get: {method:'GET', params:{iconId:''}, isArray:false},
  });
}]);
