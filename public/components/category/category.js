'use strict';

angular.module('myApp.category', [])
  .factory('Category', ['$resource',
    function($resource) {
      return $resource('/api/category', {}, {
        query: {
          method: 'GET',
          params: {
            node: ''
          },
          isArray: true
        },
      });
    }
  ])
  .factory('Subcategory', ['$resource',
    function($resource) {
      return $resource('/api/subcategory', {}, {
        query: {
          method: 'GET',
          params: {
          },
          isArray: true
        },
      });
    }
  ])
  .factory('GenKeyword', ['$resource',
    function($resource) {
      return $resource('/api/genkeyword', {}, {
        query: {
          method: 'GET',
          params: {
          },
          isArray: true
        },
        page: {
          method: 'POST',
          params: {},
          isArray: false,
        },
        refresh: {
          method: 'DELETE',
          params: {},
          isArray: false,
        }
      });
    }
  ])
  .factory('Research', ['$resource',
    function($resource) {
      return $resource('/api/research', {}, {
        get: {
          method: 'GET',
          params: {
          },
          isArray: false
        }
      });
    }
  ]);
