'use strict';

describe('myApp.viewKeyword module', function() {

  beforeEach(module('ngResource'));
  beforeEach(module('myApp.keyword'));
  beforeEach(module('myApp.viewKeyword'));

  describe('viewKeyword controller', function(){

    var scope, ctrl;
    beforeEach(inject(function($rootScope, $controller) {
      scope = $rootScope.$new();
      ctrl = $controller('viewKeywordCtrl', {$scope: scope});
    }));

    it('should test viewKeyword', (function() {
      //spec body
      var viewKeywordCtrl = ctrl;
      expect(scope.keywords).toBeDefined();
      expect(viewKeywordCtrl).toBeDefined();
    }));

  });
});