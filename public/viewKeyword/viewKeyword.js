'use strict';

angular.module('myApp.viewKeyword', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/keyword', {
    templateUrl: 'viewKeyword/viewKeyword.html',
    controller: 'viewKeywordCtrl'
  });
}])

.controller('viewKeywordCtrl', ['$scope', '$log', 'Keyword', 'Website',
  function($scope, $log, Keyword, Website) {
  $scope.keywords = [];
  $scope.keywords = Keyword.query();
  $scope.websites = Website.query();
  $scope.selectWebsite = function() {
    if ($scope.website) {
      $scope.keywords = Keyword.query({websiteId: $scope.website._id});
    } else {
      $scope.keywords = Keyword.query();
    };
  };

  $scope.delete = function(keyword) {
    Keyword.delete({_id: keyword._id}, function (res) {
      if (res.status === 'ok') {
        for (var i = $scope.keywords.length - 1; i >= 0; i--) {
          if ($scope.keywords[i]._id === keyword._id) {
            $scope.keywords.splice(i, 1);
            break;
          };
        };
      };
    });
  };
}]);