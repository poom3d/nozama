'use strict';

angular.module('myApp.viewNetwork', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider
  .when('/network', {
    templateUrl: 'viewNetwork/viewNetwork.html',
    controller: 'viewNetworkCtrl'
  })
  .when('/networkDetail', {
    templateUrl: 'viewNetwork/viewNetworkDetail.html',
    controller: 'viewNetworkDetailCtrl'
  })
  .when('/network/:networkId', {
    templateUrl: 'viewNetwork/viewNetworkDetail.html',
    controller: 'viewNetworkDetailCtrl'
  });
}])

.factory('Network', ['$resource',
function($resource){
  return $resource('/api/network/:networkId', {networkId: 'networkId'}, {
    query: {method:'GET', params:{networkId:''}, isArray:true},
    get: {method:'GET', params:{networkId:''}, isArray:false},
    put: {method:'PUT', params:{networkId:''}, isArray:false},
    post: {method:'POST', params:{networkId:''}, isArray:false},
    delete: {method:'DELETE', params:{networkId:''}, isArray:false},
  });
}])
.controller('viewNetworkCtrl', ['$scope', '$log', 'Network', function($scope, $log, Network) {
  $scope.networks = [];
  $scope.networks = Network.query();

  $scope.delete = function(network) {
    Network.delete({_id: network._id}, function (res) {
      if (res.status === 'ok') {
        for (var i = $scope.networks.length - 1; i >= 0; i--) {
          if ($scope.networks[i]._id === network._id) {
            $scope.networks.splice(i, 1);
            break;
          };

        };
      };
    });
  };
}])
.controller('viewNetworkDetailCtrl',
  ['$scope', '$log', '$routeParams', '$location', 'Network',
  function($scope, $log, $routeParams, $location, Network) {

  if ($routeParams.networkId !== undefined) {    // Update
    $scope.network = Network.get({networkId: $routeParams.networkId}, function (res) {
    });
  } else {                                      // Insert
    $scope.network = {url: ''};
  };

  $scope.save = function() {
    if ($scope.network._id !== undefined) {
      $log.log($scope.network);
      Network.put($scope.network, function(res) {
        if (res && res.status === 'ok') {
        };
      });
    } else {
      Network.post($scope.network, function(res) {
        if (res && res.status === 'ok') {
          $location.path('/network');
        };
      });
    };
  };
}]);