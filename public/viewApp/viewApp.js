'use strict';

angular.module('myApp.viewApp', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider
  .when('/app', {
    templateUrl: 'viewApp/viewApp.html',
    controller: 'viewAppCtrl'
  })
  .when('/appDetail', {
    templateUrl: 'viewApp/viewAppDetail.html',
    controller: 'viewAppDetailCtrl'
  })
  .when('/app/:appId', {
    templateUrl: 'viewApp/viewAppDetail.html',
    controller: 'viewAppDetailCtrl'
  });
}])

.factory('App', ['$resource',
function($resource){
  return $resource('/api/app/:appId', {appId: 'appId'}, {
    query: {method:'GET', params:{appId:''}, isArray:true},
    get: {method:'GET', params:{appId:''}, isArray:false},
    put: {method:'PUT', params:{appId:''}, isArray:false},
    post: {method:'POST', params:{appId:''}, isArray:false},
    delete: {method:'DELETE', params:{appId:''}, isArray:false},
  });
}])
.controller('viewAppCtrl', ['$scope', '$log', 'App',
  function($scope, $log, App) {
  $scope.apps = [];
  $scope.apps = App.query();

  $scope.delete = function(app) {
    App.delete({_id: app._id}, function (res) {
      if (res.status === 'ok') {
        for (var i = $scope.apps.length - 1; i >= 0; i--) {
          if ($scope.apps[i]._id === app._id) {
            $scope.apps.splice(i, 1);
            break;
          };

        };
      };
    });
  };
}])
.controller('viewAppDetailCtrl',
  ['$scope', '$log', '$routeParams', '$location', 'App',
  function($scope, $log, $routeParams, $location, App) {

  if ($routeParams.appId !== undefined) {    // Update
    $scope.app = App.get({appId: $routeParams.appId}, function (res) {
    });
  } else {                                      // Insert
    $scope.app = {url: ''};
  };

  $scope.save = function() {
    if ($scope.app._id !== undefined) {
      $log.log($scope.app);
      App.put($scope.app, function(res) {
        if (res && res.status === 'ok') {
        };
      });
    } else {
      App.post($scope.app, function(res) {
        if (res && res.status === 'ok') {
          $location.path('/app');
        };
      });
    };
  };
}]);