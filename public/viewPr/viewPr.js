'use strict';

angular.module('myApp.viewPr', ['ngRoute'])

.config(['$routeProvider', function ($routeProvider) {
  $routeProvider
    .when('/pr', {
      templateUrl: 'viewPr/viewPr.html',
      controller: 'viewPrCtrl'
    })
    .when('/adddomain', {
      templateUrl: 'viewPr/addDomain.html',
      controller: 'addDomainCtrl'
    });
}])

.factory('PR', ['$resource',
    function ($resource) {
      return $resource('/api/pr', {}, {
        query: {
          method: 'GET',
          params: {},
          isArray: true
        },
        add: {
          method: 'POST',
          params: {},
          isArray: false
        },
        delete: {
          method: 'DELETE',
          params: {},
          isArray: false
        }
      });
    }
  ])
  .controller('viewPrCtrl', ['$scope', '$log', 'PR',
    function ($scope, $log, PR) {
      $scope.domains = [];
      $scope.domains = PR.query();
      $scope.isCheckAll = false;

      $scope.checkall = function () {
        $log.log('test');
        for (var i = $scope.domains.length - 1; i >= 0; i--) {
          if ($scope.isCheckAll) {
            $scope.domains[i].status = true;
          } else {
            $scope.domains[i].status = false;
          };

        };
        //$scope.isCheckAll = !$scope.isCheckAll;
        $log.log($scope.domains);
      };

      $scope.markDelete = function(domain) {
        //domain.status = !domain.status;
      };

      $scope.delete = function() {

        var toDelete = [];
        var ids = [];
        for (var i = $scope.domains.length - 1; i >= 0; i--) {
          if ($scope.domains[i].status) {
            ids.push($scope.domains[i]._id);
          };
        };
        ids = ids.join(',');
        PR.delete({domains: ids}, function(res) {
          for (var i = $scope.domains.length - 1; i >= 0; i--) {
            if ($scope.domains[i].status) {
              $scope.domains.splice(i, 1);
            };

          };
        });
      };
    }
  ])
  .controller('addDomainCtrl', ['$scope', '$log', '$location', 'PR',
    function ($scope, $log, $location, PR) {
      $scope.isSubmit = false;

      $scope.save = function () {

        if ($scope.domains && $scope.domains !== '') {
          var rawDomains = $scope.domains.split('\n');
          $log.log(rawDomains);



          // Check valid url
          var expression = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
          var regex = new RegExp(expression);

          var domains = [];
          for (var i = rawDomains.length - 1; i >= 0; i--) {
            var t = rawDomains[i];
            if (t.match(regex)) {
              domains.push(t);
            } else {
              $log.log('Bad domain:' + t);
            }
          };

          // disable button
          $scope.isSubmit = true;

          PR.add({
            domains: domains
          }, function (res) {
            $log.log(res);
            $location.path('/pr');
          });
        };
      };
    }
  ]);
