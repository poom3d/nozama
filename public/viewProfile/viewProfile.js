'use strict';

angular.module('myApp.viewProfile', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider
  .when('/profile', {
    templateUrl: 'viewProfile/viewProfile.html',
    controller: 'viewProfileCtrl'
  });
}])
.factory('Profile', ['$resource',
function($resource){
  return $resource('/api/profile/', {}, {
    get: {method:'GET', params:{}, isArray:false},
    put: {method:'PUT', params:{}, isArray:false},
  });
}])
.controller('viewProfileCtrl', ['$scope', '$log', 'Profile', function($scope, $log, Profile) {
  $scope.user = {};
  $scope.trackingId = '';
  $scope.user = Profile.get(function(res) {
    $scope.trackingId = res.trackingId;
  });
  $scope.password1 = '';
  $scope.password2 = '';


  $scope.save = function() {
    if ($scope.password1 !== $scope.password2) {
      alert('Password is not matched.');
    } else {
      Profile.put({password: $scope.password1, trackingId: $scope.trackingId}, function(res) {
        alert('Profile has changed.');
      });
    }

  };

  $scope.saveTracking = function() {
    Profile.put({trackingId: $scope.trackingId}, function(res) {
      alert('Tracking ID has changed.');
    });
  };

  $scope.saveEmail = function () {
    Profile.put({email: $scope.user.email}, function(res) {
      alert('Email has changed.');
    });
  };
}]);