var request = require('request');
var http = require('http');
var cheerio = require('cheerio');
var mongoose = require('mongoose');
var kue = require('kue');

var ObjectId = mongoose.Schema.Types.ObjectId;

var Product = require('../models/product');
var Post = require('../models/post');
var Network = require('../models/network');
var KeywordCache = require('../models/keywordCache');
var Keyword = require('../models/keyword');
var User = require('../models/user');
var Website = require('../models/website');
var Domain = require('../models/domain');

var redis = require("redis");
var redisClient = redis.createClient();

var jobs = kue.createQueue();

/**
 * Get new release product list from url. Also save to mongo.
 * @param  {[type]}   url      [description]
 * @param  {Function} callback [description]
 * @return {[type]}            [description]
 */
var getNewReleases = function (url, callback) {
  request({
    url: url
  }, function onRequest(error, response, html) {
    if (!error) {
      var $ = cheerio.load(html);
      var count = 0;
      $('.product.celwidget').filter(function onProducts() {
        var data = $(this);
        var newProduct = {};

        // Asin is in the name
        newProduct.asin = data.attr('name');

        // Get image
        newProduct.imageUrl = $('.productImage', data).attr('src');

        // Get title
        newProduct.title = $('h3 .title', data).text();
        newProduct.platform = $('.platform', data).text();
        newProduct.brand = $('.ptBrand', data).text();
        newProduct.audienceRating = $('.audienceRating', data).text();


        //newProduct.rank = parseInt($('.number', data).text().replace('.', ''));
        //console.log(newProduct.title + ' rank is ' + newProduct.rank);

        //products.push(product);
        Product.findOne({
          asin: newProduct.asin
        }, function onFound(err, product) {
          if (err) {
            console.log(err);
            return;
          };

          // Create new document
          if (!product) {
            product = new Product(newProduct);
            product.save();
          } else {
            //product.rank = newProduct.rank;
            product.asin = newProduct.asin;
            product.title = newProduct.title;
            product.platform = newProduct.platform;
            product.brand = newProduct.brand;
            product.audienceRating = newProduct.audienceRating;
            product.imageUrl = newProduct.imageUrl;
            product.save();
          };
        });
        count++;
      });

      // Getting next page
      $('.pagnNext').filter(function onNextPage() {
        var data = $(this);
        var url = 'http://www.amazon.com' + data.attr('href');
        console.log(url);

        jobs.create('getNewReleases', {
          title: 'Get New Release games',
          url: url
        }).save(function (err) {
          if (!err) {
            console.log(err);
            return;
          }
        });
      });

      console.log('Number of products: ' + count);
      callback();
    } else {
      console.log(error);
      callback(error);
    }
  });
};

var getProductDetail = function (asin, callback) {
  var url = 'http://www.amazon.com/dp/' + asin;
  request({
    url: url
  }, function onRequest(error, response, html) {

    if (!error) {
      var $ = cheerio.load(html);

      var aProduct = {};
      $('.parseasinTitle').filter(function onTitle() {
        var data = $(this);
        aProduct.title = data.text().trim();
      });


      $('#actualPriceValue').filter(function onPrice() {
        aProduct.price = $(this).text().trim();
      });

      $('#SalesRank').filter(function onRank() {
        var text = $(this).text().trim();
        var regex = /#(.*) in Video Games/g;
        var result = regex.exec(text);
        if (result && result.length > 0) {
          var rawStr = result[1].replace(',', '');
          aProduct.rank = parseInt(rawStr);
          console.log('Product Rank:' + aProduct.rank);
        };
      });

      $('#product-description_feature_div').filter(function onDescription() {
        var data = $(this);

        var description = data.text().replace('View larger', '');
        description = description.replace(/\n\s*\n/g, '\n');
        description = description.replace(/View larger/g, '');
        description = description.replace(/\n/g, '<br>');

        var images = [];
        $('img', this).filter(function onImage() {
          var data = $(this);
          images.push(data.attr('src'));
        });

        aProduct.description = description;
        aProduct.images = images;
      });

      // Get Release Date
      $('#productDetailsTable').filter(function () {
        var data = $(this);

        var regex = /Release Date:(.*)/g;
        var result = regex.exec(data.text());
        if (result && result.length > 0) {
          var rawStr = result[1].trim();
          var releaseDate = new Date(result[1].trim());
          var adjustedDate = new Date(Date.UTC(releaseDate.getFullYear(),
            releaseDate.getMonth(),
            releaseDate.getDate()));

          aProduct.releaseDate = adjustedDate;
        };
      });

      return callback(undefined, aProduct);
    } else {
      return callback(error);
    };
  });
};

var getGoogleResults = function (keyword, callback) {
  var url = 'https://www.google.com/search?q=' + keyword +
    '&gbv=1&hl=en-US&gl=us';

  // Phrase match
  //'https://www.google.com/search?q=%22samsung+hero%22&btnG=Search&gbv=1'
  request({
    url: url
  }, function onRequest(error, response, html) {

    if (error) {
      return callback(error);
    };

    var $ = cheerio.load(html);

    $('#resultStats').filter(function onResult() {
      var data = $(this);
      console.log(data.text());

      var regex = /About (.*) results/g;
      var result = regex.exec(data.text());
      if (result && result.length > 0) {
        //console.log(result);
        var rawStr = result[1].replace(/,/g, '');
        //console.log(rawStr);
        var searchResults = parseInt(rawStr);
        console.log('Search Results:' + searchResults);
      } else {
        console.log(keyword);
        return callback('Cannot get Results');
      };
      return callback(undefined, searchResults);
    });

  });
  return;
};

var getMainCategories = function (callback) {
  var urls = [
    "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Daps",
    "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Dinstant-video",
    "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Dappliances",
    "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Dmobile-apps",
    "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Darts-crafts",
    "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Dautomotive",
    "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Dbaby-products",
    "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Dbeauty",
    "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Dstripbooks",
    "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Dpopular",
    "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Dmobile",
    "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Dfashion",
    "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Dfashion-womens",
    "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Dfashion-mens",
    "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Dfashion-girls",
    "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Dfashion-boys",
    "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Dfashion-baby",
    "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Dcollectibles",
    "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Dcomputers",
    "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Dfinancial",
    "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Ddigital-music",
    "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Delectronics",
    "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Dgift-cards",
    "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Dgrocery",
    "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Dhpc",
    "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Dgarden",
    "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Dindustrial",
    "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Ddigital-text",
    "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Dfashion-luggage",
    "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Dmagazines",
    "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Dmovies-tv",
    "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Dmi",
    "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Doffice-products",
    "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Dlawngarden",
    "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Dpets",
    "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Dpantry",
    "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Dsoftware",
    "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Dsporting",
    "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Dtools",
    "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Dtoys-and-games",
    "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Dvideogames",
    "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Dwine"
  ];

  var categories = [];
  for (var i = 0; i < urls.length; i++) {
    var name = urls[i].replace(
      'http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3D', '');
    categories.push({
      name: name,
      url: urls[i]
    })
  };
  return callback(categories);
};

var getSubcategories = function (url, callback) {
  redisClient.get(url, function (err, reply) {
    if (err) {
      console.log(err);
      return callback(err);
    };

    if (reply) {
      return callback(undefined, JSON.parse(reply));
    } else {

      getAmazonUrl(url, function (html) {
        var $ = cheerio.load(html);
        var categories = [];
        $('#left').filter(function onLeftNav() {
          var data = $(this);
          $('li', this).filter(function onImage() {
            var li = $(this);
            var a = $('a', li);
            //console.log(a.text());
            //console.log(a.attr('href'));
            var url = 'http://www.amazon.com' + a.attr('href');
            categories.push({
              name: a.text(),
              url: url
            })
          });
        });
        redisClient.set(url, JSON.stringify(categories), function (
          err) {
          if (err) {
            console.log(err);
            return callback(err);
          };
          return callback(undefined, categories);
        });
      });
    };
  });

};

var getKeywordsFromHtml = function (html, callback) {
  var $ = cheerio.load(html);
  var keywords = [];

  $('.s-result-item').filter(function () {
    var data = $(this);
    var keyword = {};

    // Get ASIN
    keyword.asin = data.attr('data-asin');

    // Get Title
    var title = $('.a-size-base.s-inline.s-access-title.a-text-normal',
      this);
    keyword.title = title.text().trim();

    // Get Price
    // The first one
    $('.a-size-base.a-color-price.s-price.a-text-bold', this).filter(
      function (index, element) {
        if (index == 0) {
          keyword.price = $(this).text().trim();
        };
        return;
      });

    // Get Image
    var image = $('.cfMarker', this);
    keyword.imageUrl = image.attr('src');

    // Get Free shipping
    var regex = /FREE Shipping/g;
    var result = regex.exec(data.text());
    if (result && result.length > 0) {
      keyword.freeshipping = true;
    } else {
      keyword.freeshipping = false;
    };

    // Get Fast Shipping
    regex = /Get it by /g;
    var result = regex.exec(data.text());
    if (result && result.length > 0) {
      keyword.fastshipping = true;
    } else {
      keyword.fastshipping = false;
    };

    regex = /(.*) out of 5 stars/g;
    var star = $('.a-icon-alt', this);
    var result = regex.exec(star.text());
    if (result && result.length > 0) {
      keyword.star = result[1].trim();
    } else {
      keyword.star = 0;
    };

    var keywordArray = keyword.title.split(' ');
    if (keywordArray.length > 2) {
      keyword.keyword = keywordArray[0] + ' ' + keywordArray[1];
    } else {
      keyword.keyword = keyword.title;
    }

    //keyword.permalink
    keyword.permalink = keyword.keyword.replace(/[^a-zA-Z0-9 ]/g, '');
    keyword.permalink = keyword.permalink.replace(/ /g, '-');

    keywords.push(keyword);
  });

  $('.prod.celwidget').filter(function () {
    var data = $(this);
    var keyword = {};

    // Get ASIN
    keyword.asin = data.attr('name');

    // Get Title
    var title = $('h3.newaps', this);
    keyword.title = title.text().trim();

    // Get Price
    // The first one
    $('.newp', this).each(function (index, element) {
      if (index == 0) {
        keyword.price = $(this).text().trim();
      };
      return;
    });

    // Get Image
    var image = $('.productImage.cfMarker', this);
    keyword.imageUrl = image.attr('src');

    // Get Free shipping
    var regex = /FREE Shipping/g;
    var result = regex.exec(data.text());
    if (result && result.length > 0) {
      keyword.freeshipping = true;
    } else {
      keyword.freeshipping = false;
    };

    // Get Fast Shipping
    regex = /Get it by /g;
    var result = regex.exec(data.text());
    if (result && result.length > 0) {
      keyword.fastshipping = true;
    } else {
      keyword.fastshipping = false;
    };

    regex = /alt=\"(.*) out of 5 stars/g;
    var star = $('.asinReviewsSummary', this);
    var result = regex.exec(star.html());
    if (result && result.length > 0) {
      keyword.star = result[1].trim();
    } else {
      keyword.star = 0;
    };

    var keywordArray = keyword.title.split(' ');
    if (keywordArray.length > 2) {
      keyword.keyword = keywordArray[0] + ' ' + keywordArray[1];
    } else {
      keyword.keyword = keyword.title;
    }

    //keyword.permalink
    keyword.permalink = keyword.keyword.replace(/[^a-zA-Z0-9 ]/g, '');
    keyword.permalink = keyword.permalink.replace(/ /g, '-');

    keywords.push(keyword);



  });
  //console.log(keywords);
  return callback(keywords);
};

var getKeywordsAndNextPageFromHtml = function (html, callback) {
  getKeywordsFromHtml(html, function (keywords) {
    var $ = cheerio.load(html);
    var nextPage = '';
    $('#pagnNextLink').filter(function () {
      nextPage = $(this).attr('href');
    });

    return callback(keywords, nextPage);
  });
};

var getAmazonUrl = function (url, callback) {

  if (url === undefined || url === null) {
    console.log("Got error: url is null");
    return callback('');
  };

  var path = url.replace('http://www.amazon.com', '');
  var options = {
    hostname: 'www.amazon.com',
    port: 80,
    path: path,
    method: 'GET',
    headers: {
      'User-Agent': 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)'
    }
  };

  var html = '';

  http.get(url, function (response) {
    response.on('data', function (chunk) {
      html += chunk;
    });

    response.on('end', function () {
      return callback(html);
    });
  }).on('error', function (e) {
    console.log("Got error: " + e.message);
    //return callback(e.message);
  });
};



var getKeywordsFromUrl = function (url, callback) {

  // Use node to cache instead
  var regex = /&node=(\d+)&/;
  var result = regex.exec(url);
  var node = '';
  if (result && result.length > 0) {
    node = result[1];
  };

  if (node !== '') {
    KeywordCache.find({
      node: node
    }, function (err, keywords) {
      if (err) {
        console.log(err);
        return;
      };


      if (keywords && keywords.length > 0) {
        //console.log(keywords);
        return callback(keywords);
      } else {
        getAmazonUrl(url, function (html) {
          getKeywordsFromHtml(html, function (keywords) {
            for (var i = keywords.length - 1; i >= 0; i--) {
              var keywordCache = new KeywordCache();
              //keywords[i]
              for (var key in keywords[i]) {
                if (keywords[i].hasOwnProperty(key)) {
                  //console.log("Key is " + key + ", value is" + keywords[i][key]);
                  keywordCache[key] = keywords[i][key];
                  keywords[i]._id = keywordCache._id;
                };
              };
              keywordCache.node = node;
              keywordCache.save();
            };
            return callback(keywords);
          });
        });
      };
    });
  } else {
    KeywordCache.find({
      url: url
    }, function (err, keywords) {
      if (keywords && keywords.length > 0) {
        return callback(keywords);
      } else {
        getAmazonUrl(url, function (html) {
          getKeywordsFromHtml(html, function (keywords) {
            for (var i = keywords.length - 1; i >= 0; i--) {
              var keywordCache = new KeywordCache();
              //keywords[i]
              for (var key in keywords[i]) {
                if (keywords[i].hasOwnProperty(key)) {
                  //console.log("Key is " + key + ", value is" + keywords[i][key]);
                  keywordCache[key] = keywords[i][key];
                  keywords[i]._id = keywordCache._id;
                };
              };
              keywordCache.url = url;
              keywordCache.save();
            };
            return callback(keywords);
          });
        });
      };
    });
  };
};

var updateKeywordCache = function (keyword, callback) {
  console.log(keyword);
  KeywordCache.findOne({
    _id: keyword._id
  }, function (err, keywordCache) {
    if (err) {
      console.log(err);
      return callback(err);
    };

    if (keywordCache) {
      for (var key in keyword) {
        if (keyword.hasOwnProperty(key)) {
          keywordCache[key] = keyword[key];
        };

        if (key === 'googleResults') {
          console.log(keyword[key]);
        };
      };

      keywordCache.save(function (err) {
        if (err) {
          console.log(err);
          return callback(err);
        };

        return callback(undefined, keywordCache);
      });
    } else {
      return callback({
        errMsg: 'cannot find keyword cache'
      });
    }
  });
};

var validateSession = function (req, callback) {
  var userId = req.session.uid;

  if (userId === undefined) {
    userId = req.signedCookies.uid;
    console.log(userId);
  } else {
    console.log('got something from session');
  }

  if (userId !== undefined && typeof (userId) !== 'undefined') {
    User.findOne({
        _id: userId
      }, '_id username email package status term dueDate trackingId',
      function (err, user) {
        if (err) {
          console.log(err);
          return callback({
            status: 'error',
            errorMsg: 'database'
          });
        };

        var currentDate = new Date();
        if (user.status !== 'admin' && user.dueDate < currentDate) {
          user.status = 'unpaid';
          user.save();
        };

        if (user && (user.status === 'paid' || user.status === 'unpaid')) {
          return callback(undefined, user);
        } else if (user && user.status === 'admin') {
          return callback(undefined, user);
        } else {
          return callback({
            status: 'error',
            errorMsg: 'not found'
          });
        };
      });
  } else {
    return callback({
      status: 'error',
      errorMsg: 'not found'
    });
  }
};

var createPosts = function (asin, user, callback) {
  var url = 'http://www.amazon.com/product-reviews/' + asin;
  // Create Jobs for getting reviews
  jobs.create('getNextPageReviews', {
    title: 'Get Reviews',
    url: url,
    asin: asin,
    user: user
  }).save(function onJobCreated(err) {
    if (err) {
      console.log(err);
      return callback(err);
    };

    callback(undefined, {
      status: 'OK'
    });
  });
};

var createNetworkJobs = function (callback) {

  Post.findOne({
    status: 'pending'
  }, function (err, post) {



    post.status = 'complete';
    post.markModified('status');
    post.save();

    var newPost = {};
    newPost.title = post.title;
    newPost.content = post.content;
    newPost.status = 'publish';
    Network.find({}, function (err, networks) {
      if (err) {
        console.log(err);
        return callback(err);
      };

      for (var i = networks.length - 1; i >= 0; i--) {
        jobs.create('postToNetwork', {
          title: 'Posting to Network',
          network: networks[i],
          post: newPost
        }).save(function onJobCreated(err) {
          if (err) {
            console.log(err);
            return callback(err);
          };
        });
      };

      callback(undefined);

    });
  });
};


// Now it's getting messy
var getWebsite = function (url, callback) {
  Website.findOne({
    url: url
  }, function (err, website) {

    if (err) {
      console.log(err);
      return callback(err);
    };

    if (website) {
      var template = 'default';

      return callback(null, website);

    } else {
      return callback({
        status: 'error',
        errorMsg: 'Not Found'
      });
    };
  });
};

var generateSitemapXML = function (url, callback) {
  // this is the source of the URLs on your site.
  Website.findOne({
    url: url
  }, function (err, website) {
    if (err) {
      return callback(err);
    };

    if (website) {
      Keyword.find({
        website: website._id
      }, function (err, keywords) {
        if (err) {
          return callback(err);
        };

        //console.log(keywords);
        var urls = ['', 'new-releases', 'recent-update', 'about-us',
          'contact-us', 'tos', 'privacy'
        ];
        for (var i = keywords.length - 1; i >= 0; i--) {
          if (keywords[i].permalink) {
            urls.push(keywords[i].permalink);
          };
        };

        console.log(urls.length);
        // the root of your website - the protocol and the domain name with a trailing slash
        var root_path = 'http://' + url + '/';
        // XML sitemap generation starts here
        var priority = 0.5;
        var freq = 'daily';
        var xml =
          '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
        for (var j = urls.length - 1; j >= 0; j--) {
          xml += '<url>';
          xml += '<loc>' + root_path + urls[i] + '</loc>';
          xml += '<changefreq>' + freq + '</changefreq>';
          xml += '<priority>' + priority + '</priority>';
          xml += '</url>';
          i++;
        };
        xml += '</urlset>';
        return callback(null, xml);
      });
    } else {
      return callback({
        status: 'error',
        errorMsg: 'No Site Found'
      });
    }

  });
};

var deleteKeywordCache = function (callback) {
  KeywordCache.remove({}, function (err) {
    if (err) {
      return callback(err);
    };

    return callback(null);
  });
};


// Each user will have their own url list
// But pr will has only one copy
var createCheckPrJobs = function (user, urls, callback) {
  for (var i = urls.length - 1; i >= 0; i--) {
    //urls[i]
    var newDomain = new Domain();
    var url = urls[i];
    newDomain.url = urls[i];
    newDomain.userId = user._id;
    var milliseconds = Math.floor((Math.random() * 3000) + 1000);
    newDomain.save();

    // Create jobs
    console.log('Create a job for ' + url);
    jobs.create('getPRforDomain', {
      title: 'Get PR for ' + url,
      url: url,
      domainId: newDomain._id
    }).delay(milliseconds).save(function onJobCreated(err) {
      if (err) {
        console.log(err);
        return callback(err);
      };
    });
    milliseconds += Math.floor((Math.random() * 3000) + 1000);
  };

  return callback(null);
};

var createAutoGenJobs = function (website, url, callback) {
  jobs.create('getGenKeywords', {
    title: 'Generate keywords from ' + url,
    url: url,
    website: website
  }).save(function onJobCreated(err) {
    if (err) {
      console.log(err);
      return callback(err);
    };

    return callback(null);
  });
};

exports.getNewReleases = getNewReleases;
exports.getProductDetail = getProductDetail;
exports.getGoogleResults = getGoogleResults;
exports.getMainCategories = getMainCategories;
exports.getSubcategories = getSubcategories;
exports.getKeywordsFromHtml = getKeywordsFromHtml;
exports.getKeywordsAndNextPageFromHtml = getKeywordsAndNextPageFromHtml;
exports.getAmazonUrl = getAmazonUrl;
exports.getKeywordsFromUrl = getKeywordsFromUrl;
exports.updateKeywordCache = updateKeywordCache;
exports.validateSession = validateSession;
exports.createPosts = createPosts;
exports.createNetworkJobs = createNetworkJobs;
exports.getWebsite = getWebsite;
exports.generateSitemapXML = generateSitemapXML;
exports.deleteKeywordCache = deleteKeywordCache;
exports.createCheckPrJobs = createCheckPrJobs;
exports.createAutoGenJobs = createAutoGenJobs;
