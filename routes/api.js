var express = require('express');
var request = require('request');
var cheerio = require('cheerio');
var helper = require('./helper');
var router = express.Router();
var Product = require('../models/product');
var Website = require('../models/website');
var Network = require('../models/network');
var Keyword = require('../models/keyword');
var Post = require('../models/post');
var Icon = require('../models/icon');
var Domain = require('../models/domain');
var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;

var wordpress = require("wordpress");


// Do nothing
router.get('/', function (req, res) {
  return res.send('OK');
});

/* GET keyword list */
router.get('/keyword', function getkeywordList(req, res) {
  helper.validateSession(req, function (err, user) {
    if (err) {
      return res.send({
        status: 'silence'
      });
    };

    var websiteId = req.query.websiteId;
    if (websiteId !== undefined) {
      Keyword.find({
        userId: user._id,
        website: websiteId
      }, function (err, keywords) {
        if (err) {
          console.log(err);
          return;
        };
        return res.send(keywords);
      });
    } else {
      Keyword.find({
        userId: user._id
      }, function (err, keywords) {
        if (err) {
          console.log(err);
          return;
        };
        return res.send(keywords);
      });
    };
  });
});

// Insert keyword
router.post('/keyword', function saveKeyword(req, res) {

  helper.validateSession(req, function (err, user) {
    if (err) {
      return res.send({
        status: 'silence'
      });
    };

    if (req.body._id !== undefined) {
      delete req.body._id;
    };

    var keyword = new Keyword(req.body);
    keyword.userId = user._id;
    // if (keyword.website !== undefined) {
    keyword.save(function (err) {
      if (err) {
        console.log(err);
      };
    });
    return res.send({
      status: 'ok'
    });
    // } else {
    //   return res.send({
    //     status: 'error'
    //   });
    // };
  });
});

// Get keyword detail
router.get('/keyword/:keywordId', function getKeywordDetail(req, res) {
  var keywordId = req.params.keywordId;
  helper.validateSession(req, function (err, user) {
    if (err) {
      return res.send({
        status: 'silence'
      });
    };
    Keyword.findOne({
      _id: keywordId,
      userId: user._id
    }, function (err, keyword) {
      if (err) {
        console.log(err);
        return;
      };

      console.log(keyword);
      return res.send(keyword);
    });
  });
});

// update keyword
router.put('/keyword', function updateKeyword(req, res) {
  helper.validateSession(req, function (err, user) {
    if (err) {
      return res.send({
        status: 'silence'
      });
    };

    Keyword.findOne({
      _id: req.body._id,
      userId: user._id
    }, function (err, keyword) {
      if (err) {
        console.log(err);
        return res.send({
          status: 'error'
        });
      };

      if (keyword.userId.toString() === user._id.toString()) {

        for (var key in req.body) {
          if (req.body.hasOwnProperty(key) && key !== '_id') {
            keyword[key] = req.body[key];
          };
        };
        keyword.updated = new Date();
        keyword.save();
        return res.send({
          status: 'ok'
        });
      } else {
        return res.send({
          status: 'error',
          errorMsg: 'interesting'
        });
      };
    });
  });
});

// delete keyword
router.delete('/keyword', function deleteKeyword(req, res) {
  helper.validateSession(req, function (err, user) {
    if (err) {
      console.log(err);
      return res.send({
        status: 'silence'
      });
    };

    Keyword.findOne({
      _id: req.query._id
    }, function (err, keyword) {
      if (err) {
        console.log(err);
        return res.send({
          status: 'error'
        });
      };

      if (keyword.userId.toString() === user._id.toString()) {
        keyword.remove();
        return res.send({
          status: 'ok'
        });
      } else {
        return res.send({
          status: 'error',
          errorMsg: 'interesting'
        });
      };
    });
  });
});




// Website query
router.get('/website', function (req, res) {
  helper.validateSession(req, function (err, user) {
    if (err) {
      return res.send({
        status: 'silence'
      });
    };

    if (user.username === 'admin') {
      Website.find({}).populate('userId logo').exec(function (err,
        websites) {
        if (err) {
          console.log(err);
          return;
        };
        return res.send(websites);
      });
    } else {
      Website.find({
        userId: user._id
      }).populate('userId').exec(function (err, websites) {
        if (err) {
          console.log(err);
          return;
        };
        return res.send(websites);
      });
    };


  });
});

router.get('/website/:websiteId', function (req, res) {
  helper.validateSession(req, function (err, user) {
    if (err) {
      return res.send({
        status: 'silence'
      });
    };

    if (user.username === 'admin') {
      Website.findOne({
        _id: req.params.websiteId
      }).populate('logo').exec(function (err, website) {
        if (err) {
          console.log(err);
          return;
        };
        return res.send(website);
      });
    } else {
      Website.findOne({
        userId: user._id.toString(),
        _id: req.params.websiteId
      }).populate('logo').exec(function (err, website) {
        if (err) {
          console.log(err);
          return;
        };
        return res.send(website);
      });
    };


  });
});

// Website Save
router.post('/website', function insertWebsite(req, res) {

  helper.validateSession(req, function (err, user) {
    if (err) {
      return res.send({
        status: 'silence'
      });
    };

    if (user.package === 'standard') {
      Website.count({
        userId: user._id
      }, function (err, c) {
        if (err) {
          console.log(err);
          return res.send({
            status: 'silence'
          });
        };

        if (c < 10) {
          var url = req.body.url;
          Website.findOne({
            url: url
          }, function (err, website) {
            if (err) {
              console.log(err);
              return res.send({
                status: 'error'
              });
            };

            if (website) {
              return res.send({
                status: 'error',
                errorMsg: 'Duplicate Website URL'
              });
            };

            var newWebsite = new Website(req.body);
            newWebsite.userId = user._id;
            newWebsite.save();
            return res.send({
              status: 'ok'
            });
          });

        } else {
          return res.send({
            status: 'limit',
            errorMsg: 'limit exceed'
          });
        };
      });
    } else {
      var url = req.body.url;
      Website.findOne({
        url: url
      }, function (err, website) {
        if (err) {
          console.log(err);
          return res.send({
            status: 'error'
          });
        };

        if (website) {
          return res.send({
            status: 'error',
            errorMsg: 'Duplicate Website URL'
          });
        };

        var newWebsite = new Website(req.body);
        newWebsite.userId = user._id;
        newWebsite.save();
        return res.send({
          status: 'ok'
        });
      });
    };

  });
});

// Website Update
router.put('/website', function updateWebsite(req, res) {
  helper.validateSession(req, function (err, user) {
    if (err) {
      return res.send({
        status: 'silence'
      });
    };

    Website.findOne({
      _id: req.body._id
    }, function (err, website) {
      if (err) {
        console.log(err);
        return res.send({
          status: 'error'
        });
      };

      if (website.userId.toString() === user._id.toString() ||
        user.username === 'admin') {
        website.url = req.body.url;
        website.title = req.body.title;
        website.description = req.body.description;
        website.trackingId = req.body.trackingId;
        website.logo = req.body.logo;
        website.markModified('title');
        website.markModified('description');
        website.markModified('trackingId');
        website.save();
        return res.send({
          status: 'ok'
        });
      } else {
        return res.send({
          status: 'error',
          errorMsg: 'interesting'
        });
      };
    });
  });
});

// Website Delete
router.delete('/website', function deleteWebsite(req, res) {
  helper.validateSession(req, function (err, user) {
    if (err) {
      console.log(err);
      return res.send({
        status: 'silence'
      });
    };

    Website.findOne({
      _id: req.query._id
    }, function (err, website) {
      if (err) {
        console.log(err);
        return res.send({
          status: 'error'
        });
      };

      if (website.userId.toString() === user._id.toString()) {
        website.remove();
        return res.send({
          status: 'ok'
        });
      } else {
        return res.send({
          status: 'error',
          errorMsg: 'interesting'
        });
      };
    });
  });
});

// Icon
router.get('/icon', function onQueryIcon(req, res) {
  helper.validateSession(req, function (err, user) {
    if (err) {
      return res.send({
        status: 'silence'
      });
    };

    Icon.find({}, function (err, icons) {
      if (err) {
        console.log(err);
        return;
      };
      return res.send(icons);
    });

  });
});

// Icon detail
router.get('/icon/:iconId', function onGetIcon(req, res) {
  helper.validateSession(req, function (err, user) {
    if (err || user.username !== 'admin') {
      return res.send({
        status: 'silence'
      });
    };

    Icon.findOne({
      _id: req.params.iconId
    }, function (err, icon) {
      if (err) {
        console.log(err);
        return;
      };
      console.log(icon);
      return res.send(icon);
    });
  });
});

// Icon Save
router.post('/icon', function insertIcon(req, res) {

  helper.validateSession(req, function (err, user) {
    if (err || user.username !== 'admin') {
      return res.send({
        status: 'silence'
      });
    };

    var newIcon = new Icon(req.body);
    newIcon.save();
    return res.send({
      status: 'ok'
    });
  });
});

// Icon Update
router.put('/icon', function updateIcon(req, res) {
  helper.validateSession(req, function (err, user) {
    if (err) {
      return res.send({
        status: 'silence'
      });
    };

    Icon.findOne({
      _id: req.body._id
    }, function (err, icon) {
      if (err || user.username !== 'admin') {
        console.log(err);
        return res.send({
          status: 'error'
        });
      };


      icon.url = req.body.url;
      icon.name = req.body.name;
      icon.attribution = req.body.attribution;
      icon.markModified('url');
      icon.markModified('name');
      icon.markModified('attribution');
      icon.save();
      return res.send({
        status: 'ok'
      });

    });
  });
});

// Icon Delete
router.delete('/icon', function deleteIcon(req, res) {
  helper.validateSession(req, function (err, user) {
    if (err || user.username !== 'admin') {
      console.log(err);
      return res.send({
        status: 'silence'
      });
    };

    Icon.findOne({
      _id: req.query._id
    }, function (err, icon) {
      if (err) {
        console.log(err);
        return res.send({
          status: 'error'
        });
      };

      icon.remove();
      return res.send({
        status: 'ok'
      });
    });
  });
});

// Network
router.get('/network', function onQueryNetwork(req, res) {
  helper.validateSession(req, function (err, user) {
    if (err) {
      return res.send({
        status: 'silence'
      });
    };

    Network.find({
      userId: user._id
    }, function (err, websites) {
      if (err) {
        console.log(err);
        return;
      };
      return res.send(websites);
    });
  });
});

router.get('/network/:networkId', function onGetNetworkDetail(req, res) {
  helper.validateSession(req, function (err, user) {
    if (err) {
      return res.send({
        status: 'silence'
      });
    };

    Network.findOne({
      userId: user._id.toString(),
      _id: req.params.networkId
    }, function (err, network) {
      if (err) {
        console.log(err);
        return;
      };
      console.log(network);
      return res.send(network);
    });
  });
});

// Network Save
router.post('/network', function insertNetwork(req, res) {

  helper.validateSession(req, function (err, user) {
    if (err) {
      return res.send({
        status: 'silence'
      });
    };

    console.log(req.body);
    var newNetwork = new Network(req.body);
    newNetwork.userId = user._id;
    newNetwork.save();
    return res.send({
      status: 'ok'
    });
  });
});

// Network Update
router.put('/network', function updateNetwork(req, res) {
  helper.validateSession(req, function (err, user) {
    if (err) {
      return res.send({
        status: 'silence'
      });
    };

    Network.findOne({
      _id: req.body._id
    }, function (err, network) {
      if (err) {
        console.log(err);
        return res.send({
          status: 'error'
        });
      };

      if (network.userId.toString() === user._id.toString()) {
        network.url = req.body.url;
        network.username = req.body.username;
        network.password = req.body.password;
        network.markModified('username');
        network.markModified('password');
        network.save();
        return res.send({
          status: 'ok'
        });
      } else {
        return res.send({
          status: 'error',
          errorMsg: 'interesting'
        });
      };
    });
  });
});

// Network Delete
router.delete('/network', function deleteNetwork(req, res) {
  helper.validateSession(req, function (err, user) {
    if (err) {
      console.log(err);
      return res.send({
        status: 'silence'
      });
    };

    Network.findOne({
      _id: req.query._id
    }, function (err, network) {
      if (err) {
        console.log(err);
        return res.send({
          status: 'error'
        });
      };

      if (network.userId.toString() === user._id.toString()) {
        network.remove();
        return res.send({
          status: 'ok'
        });
      } else {
        return res.send({
          status: 'error',
          errorMsg: 'interesting'
        });
      };
    });
  });
});

// Post
router.get('/post', function onQueryPost(req, res) {
  helper.validateSession(req, function (err, user) {
    if (err) {
      return res.send({
        status: 'silence'
      });
    };

    Post.find({
      //userId: user._id
    }, function (err, posts) {
      if (err) {
        console.log(err);
        return;
      };
      return res.send(posts);
    });
  });
});

router.post('/post', function onCreatePost(req, res) {
  var asin = req.body.asin;
  helper.validateSession(req, function (err, user) {
    if (err) {
      return res.send({
        status: 'silence'
      });
    };

    helper.createPosts(asin, user, function (err, result) {
      if (err) {
        console.log('error:' + err);
      };
      res.send(result);
    });
  });
});

router.delete('/post', function onDeletePost(req, res) {
  helper.validateSession(req, function (err, user) {
    if (err) {
      console.log(err);
      return res.send({
        status: 'silence'
      });
    };

    Post.findOne({
      _id: req.query._id
    }, function (err, post) {
      if (err) {
        console.log(err);
        return res.send({
          status: 'error'
        });
      };

      post.remove();
      return res.send({
        status: 'ok'
      });
    });
  });
});

// Profile
router.get('/profile', function onGetProfile(req, res) {
  helper.validateSession(req, function (err, user) {
    if (err) {
      return res.send({
        status: 'silence'
      });
    };
    return res.send(user);
  });
});

router.put('/profile', function onUpdateProfile(req, res) {
  helper.validateSession(req, function (err, user) {
    if (err) {
      return res.send({
        status: 'silence'
      });
    };
    if (req.body.password !== undefined) {
      user.password = req.body.password;
    };

    if (req.body.email !== undefined) {
      user.email = req.body.email;
      user.markModified('email');
    };

    if (req.body.trackingId !== undefined) {
      user.trackingId = req.body.trackingId;
      user.markModified('trackingId');
    };

    user.save();
    return res.send({
      status: 'ok'
    });
  });
});


// Intelligent
router.get('/category', function getCategoryList(req, res) {

  helper.validateSession(req, function (err, user) {
    if (err) {
      console.log(err);
      return res.send({
        status: 'silence'
      });
    };
    helper.getMainCategories(function (categories) {
      return res.send(categories);
    });
  });
});

router.get('/subcategory', function getSubcategoryList(req, res) {
  var url = req.query.url;


  helper.validateSession(req, function (err, user) {
    if (err) {
      console.log(err);
      return res.send({
        status: 'silence'
      });
    };
    helper.getSubcategories(url, function (err, categories) {
      if (err) {
        console.log(err);
        return;
      };
      return res.send(categories);
    });
  });
});


// Gen keyword => should be renamed
router.get('/genkeyword', function getKeywordsFromUrl(req, res) {
  var url = req.query.url;

  helper.validateSession(req, function (err, user) {
    if (err) {
      console.log(err);
      return res.send({
        status: 'silence'
      });
    };
    helper.getKeywordsFromUrl(url, function (keywords) {
      return res.send(keywords);
    });
  });
});

router.post('/genkeyword', function getKeywordsPage(req, res) {
  var url = req.body.url;
  helper.validateSession(req, function (err, user) {
    if (err) {
      console.log(err);
      return res.send({
        status: 'silence'
      });
    };
    helper.getAmazonUrl(url, function (html) {
      var $ = cheerio.load(html);
      var page = {};
      $('#pagnNextLink').filter(function () {
        page.next = 'http://www.amazon.com' + $(this).attr(
          'href');
      });
      $('#pagnPrevLink').filter(function () {
        page.prev = 'http://www.amazon.com' + $(this).attr(
          'href');
      });
      return res.send(page);
    });
  });
});

router.delete('/genkeyword', function onRefreshKeyword(req, res) {
  helper.validateSession(req, function (err, user) {
    if (err) {
      console.log(err);
      return res.send({
        status: 'silence'
      });
    };

    helper.deleteKeywordCache(function (err) {
      if (err) {
        console.log(err);
        return res.send({
          status: 'error'
        });
      };
      return res.send({
        status: 'ok'
      });
    });
  });
});

// Research google results
router.get('/research', function (req, res) {
  var keyword = req.query.keyword;
  var _id = req.query._id;
  helper.getGoogleResults(keyword, function onComplete(err, results) {
    if (err) {
      console.log(err);
      return;
    };
    var keywordCache = {
      _id: _id,
      keyword: keyword,
      googleResults: parseInt(results)
    };
    console.log(keywordCache);
    helper.updateKeywordCache(keywordCache, function (err, keywordCache) {
      if (err) {
        console.log(err);
        return;
      };
      return res.send({
        googleResults: results
      });
    });

  });
});

router.get('/review/:asin', function onGetReview(req, res) {
  var asin = req.params.asin;
  console.log(asin);

  var html = '';

  var url = 'http://www.amazon.com/dp/' + asin;


  helper.getAmazonUrl(url, function onGetDetail(html) {
    var $ = cheerio.load(html);
    var reviews = [];
    $('#revMHRL').filter(function (index, element) {
      var data = $(this);
      console.log('1 it goes here');
      $('.drkgry', this).filter(function (index, element) {
        if ($(this).attr('id')) {
          //return;
          // Do nothing
        } else {
          if ($(this).text() !==
            'Was this review helpful to you?') {
            reviews.push($(this).text().trim());
          };
        };
      });

      res.send({
        status: 'ok',
        reviews: reviews
      });

    });

  });

});

// PR
router.get('/pr', function onQueryPr(req, res) {
  helper.validateSession(req, function (err, user) {
    if (err) {
      console.log(err);
      return res.send({
        status: 'silence'
      });
    };

    Domain.find({
      userId: user._id
    }).populate('pr').sort('-pr').lean().exec(function (err, domains) {
      if (err) {
        console.log(err);

        return res.send({
          status: 'silence'
        });
      };

      for (var i = domains.length - 1; i >= 0; i--) {
        if (domains[i].pr) {
          domains[i].pr = domains[i].pr.pr;
        };
      };
      return res.send(domains);
    });
  });
});

router.post('/pr', function onPostPr(req, res) {
  console.log(req.body.domains);
  helper.validateSession(req, function (err, user) {
    if (err) {
      console.log(err);
      return res.send({
        status: 'silence'
      });
    };

    // createCheckPrJobs
    // Should validate again
    helper.createCheckPrJobs(user, req.body.domains, function (err) {
      if (err) {
        console.log(err);
        return res.send({
          status: 'error'
        });
      };

      return res.send({
        status: 'ok'
      });
    });
  });
});

router.delete('/pr', function onDeletePr(req, res) {
  helper.validateSession(req, function (err, user) {
    if (err) {
      console.log(err);
      return res.send({
        status: 'silence'
      });
    };

    var ids = req.query.domains.split(',');
    Domain.remove({
      userId: user._id,
      _id: {
        $in: ids
      }
    }, function (err) {
      if (err) {
        console.log(err);
        return res.send({
          status: 'silence'
        });
      };

      return res.send({
        status: 'ok'
      });
    });
  });
});

// Autogen
router.post('/gen', function onGen(req, res) {
  console.log(req.body);

  helper.validateSession(req, function (err, user) {
    if (err) {
      console.log(err);
      return res.send({
        status: 'silence'
      });
    };

    helper.createAutoGenJobs(req.body.website, req.body.url, function (err) {
      if (err) {
        console.log(err);
        return res.send({
          status: 'silence'
        });
      };

      return res.send({
        status: 'ok'
      });
    }); // createAutoGenJobs
  }); // validateSession
});

// App *******************************************************
var App = require('../models/app');

router.get('/publicapp', function queryPublicApp(req, res) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Credentials', true);
  res.header('Access-Control-Allow-Methods', 'POST, GET');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  var cat = req.query.cat;
  App.find({
    cat: {$ne: cat}
  }).limit(3).exec(function (err, apps) {
    if (err) {
      console.log(err);
      return;
    };
    return res.send(apps);
  });
});

router.get('/app', function queryApp(req, res) {
  helper.validateSession(req, function (err, user) {
    if (err) {
      return res.send({
        status: 'silence'
      });
    };

    App.find({
      userId: user._id
    }, function (err, apps) {
      if (err) {
        console.log(err);
        return;
      };
      return res.send(apps);
    });
  });
});

router.get('/app/:networkId', function getApp(req, res) {
  helper.validateSession(req, function (err, user) {
    if (err) {
      return res.send({
        status: 'silence'
      });
    };

    App.findOne({
      userId: user._id.toString(),
      _id: req.params.networkId
    }, function (err, app) {
      if (err) {
        console.log(err);
        return;
      };
      console.log(app);
      return res.send(app);
    });
  });
});

// App Save
router.post('/app', function postApp(req, res) {

  helper.validateSession(req, function (err, user) {
    if (err) {
      return res.send({
        status: 'silence'
      });
    };

    console.log(req.body);
    var newApp = new App(req.body);
    newApp.userId = user._id;
    newApp.save();
    return res.send({
      status: 'ok'
    });
  });
});

// App Update
router.put('/app', function putApp(req, res) {
  helper.validateSession(req, function (err, user) {
    if (err) {
      return res.send({
        status: 'silence'
      });
    };

    App.findOne({
      _id: req.body._id
    }, function (err, app) {
      if (err) {
        console.log(err);
        return res.send({
          status: 'error'
        });
      };

      if (app.userId.toString() === user._id.toString()) {
        app.name = req.body.name;
        app.summary = req.body.summary;
        app.cat = req.body.cat;
        app.ios = req.body.ios;
        app.android = req.body.android;
        app.markModified('name');
        app.markModified('summary');
        app.markModified('cat');
        app.markModified('ios');
        app.markModified('android');

        app.save();
        return res.send({
          status: 'ok'
        });
      } else {
        return res.send({
          status: 'error',
          errorMsg: 'interesting'
        });
      };
    });
  });
});

// App Delete
router.delete('/app', function deleteApp(req, res) {
  helper.validateSession(req, function (err, user) {
    if (err) {
      console.log(err);
      return res.send({
        status: 'silence'
      });
    };

    App.findOne({
      _id: req.query._id
    }, function (err, app) {
      if (err) {
        console.log(err);
        return res.send({
          status: 'error'
        });
      };

      if (app.userId.toString() === user._id.toString()) {
        app.remove();
        return res.send({
          status: 'ok'
        });
      } else {
        return res.send({
          status: 'error',
          errorMsg: 'interesting'
        });
      };
    });
  });
});

// *************************************************

// Cron job for auto post
/*
var CronJob = require('cron').CronJob;
var job1 = new CronJob({
  // Sec Min Hour * * *
  cronTime: '00 37 07 * * *',
  onTick: function () {
    helper.createNetworkJobs(function (err) {
      if (err) {
        console.log(err);
        return;
      };
      return;
    });
  },
  start: false,
});

var job2 = new CronJob({
  // Sec Min Hour * * *
  cronTime: '00 07 12 * * *',
  onTick: function () {
    helper.createNetworkJobs(function (err) {
      if (err) {
        console.log(err);
        return;
      };
      return;
    });
  },
  start: false,
});

var job3 = new CronJob({
  // Sec Min Hour * * *
  cronTime: '00 37 18 * * *',
  onTick: function () {
    helper.createNetworkJobs(function (err) {
      if (err) {
        console.log(err);
        return;
      };
      return;
    });
  },
  start: false,
});

job1.start();
job2.start();
job3.start();

*/

module.exports = router;
