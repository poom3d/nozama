var express = require('express');
var path = require('path');
var paypal = require('paypal-rest-sdk');
var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;

var config = {};
var router = express.Router();
var Product = require('../models/product');
var Website = require('../models/website');
var Keyword = require('../models/keyword');
var Article = require('../models/article');
var User = require('../models/user');

var helper = require('./helper');

var redis = require("redis");
var redisClient = redis.createClient();

var returnUrl = 'http://localhost:3000/execute';
var cancelUrl = 'http://localhost:3000/cancel';

if ('production' === process.env.NODE_ENV) {
  console.log('we go live');
  returnUrl = 'https://nozama.io/execute';
  cancelUrl = 'https://nozama.io/cancel';
};

// Payment for test
var payment = {
  "intent": "sale",
  "payer": {
    "payment_method": "paypal"
  },
  "redirect_urls": {
    "return_url": returnUrl,
    "cancel_url": cancelUrl
  },
  "transactions": [{
    "amount": {
      "total": "9.99",
      "currency": "USD"
    },
    "description": "Standard Package"
  }]
};

router.post('/signup', function onSignupPost(req, res) {
  console.log(req.body);

  // Create user
  User.findOne({
    username: req.body.username
  }, function (err, user) {
    if (err) {
      console.log(err);
      return res.redirect('/signup?error=database');;
    };

    if (user) {
      return res.redirect('/signup?error=username');
    };
  });

  // Create payment
  var amount = '';
  var package = 'standard';
  if (req.body.package === 'standard') {
    if (req.body.term === 'monthly') {
      amount = '9.99';
    } else if (req.body.term === 'yearly') {
      amount = '107.89';
    } else {
      return res.redirect('/signup?error=term');
    };
  } else if (req.body.package === 'premium') {
    package = 'Premium';
    if (req.body.term === 'monthly') {
      amount = '49.99';
    } else if (req.body.term === 'yearly') {
      amount = '539.89';
    } else {
      return res.redirect('/signup?error=term');
    };
  } else {
    return res.redirect('/signup?error=package');
  }
  var payment = {
    "intent": "sale",
    "payer": {
      "payment_method": "paypal"
    },
    "redirect_urls": {
      "return_url": returnUrl,
      "cancel_url": cancelUrl
    },
    "transactions": [{
      "amount": {
        "total": amount,
        "currency": "USD"
      },
      "description": package + " Package"
    }]
  };
  paypal.payment.create(payment, function (error, payment) {
    if (error) {
      console.log(error);
    } else {
      if (payment.payer.payment_method === 'paypal') {
        req.session.paymentId = payment.id;

        var newUser = new User(req.body);
        newUser.paymentId = payment.id;
        newUser.save();
        var redirectUrl;
        for (var i = 0; i < payment.links.length; i++) {
          var link = payment.links[i];
          if (link.method === 'REDIRECT') {
            redirectUrl = link.href;
          }
        }
        return res.redirect(redirectUrl);
      }
    };
  });
});

// For payment, copy above
router.get('/pay', function onPay(req, res) {
  var term = req.query.term;


  helper.validateSession(req, function (err, user) {
    if (err) {
      console.log(err);
      return res.redirect('/pay?error=' + err.status);
    };

    // Findout amount
    var amount = '';
    if (user.package === 'standard') {
      if (term === 'monthly') {
        amount = '9.99';
      } else if (term === 'yearly') {
        amount = '107.89';
      } else {
        return res.redirect('/pay?error=term');
      };
    } else if (user.package === 'premium') {
      package = 'Premium';
      if (term === 'monthly') {
        amount = '49.99';
      } else if (term === 'yearly') {
        amount = '539.89';
      } else {
        return res.redirect('/pay?error=term');
      };
    } else {
      return res.redirect('/pay?error=package');
    };

    // Create payment
    var payment = {
      "intent": "sale",
      "payer": {
        "payment_method": "paypal"
      },
      "redirect_urls": {
        "return_url": returnUrl,
        "cancel_url": cancelUrl
      },
      "transactions": [{
        "amount": {
          "total": amount,
          "currency": "USD"
        },
        "description": user.package + " Package"
      }]
    };
    paypal.payment.create(payment, function (error, payment) {
      if (error) {
        console.log(error);
      } else {
        if (payment.payer.payment_method === 'paypal') {
          req.session.paymentId = payment.id;


          user.paymentId = payment.id;
          // also update term
          user.term = term;
          user.save();
          var redirectUrl;
          for (var i = 0; i < payment.links.length; i++) {
            var link = payment.links[i];
            if (link.method === 'REDIRECT') {
              redirectUrl = link.href;
            }
          }
          return res.redirect(redirectUrl);
        }
      };
    });
  });
});

router.get('/upgrade', function onUpgrade(req, res) {
  var term = req.query.term;
  helper.validateSession(req, function (err, user) {
    if (err) {
      console.log(err);
      return res.redirect('/admin?error=' + err.status);
    };

    // Findout amount
    var amount = '';
    var package = 'Premium';
    if (term === 'monthly') {
      amount = '49.99';
    } else if (term === 'yearly') {
      amount = '539.89';
    } else {
      return res.redirect('/admin?error=term');
    };

    // Create payment
    var payment = {
      "intent": "sale",
      "payer": {
        "payment_method": "paypal"
      },
      "redirect_urls": {
        "return_url": returnUrl,
        "cancel_url": cancelUrl
      },
      "transactions": [{
        "amount": {
          "total": amount,
          "currency": "USD"
        },
        "description": package + " Package Upgrade"
      }]
    };
    paypal.payment.create(payment, function (error, payment) {
      if (error) {
        console.log(error);
      } else {
        if (payment.payer.payment_method === 'paypal') {
          req.session.paymentId = payment.id;


          user.paymentId = payment.id;
          // also update term
          user.package = 'premium';
          user.term = term;
          user.save();
          var redirectUrl;
          for (var i = 0; i < payment.links.length; i++) {
            var link = payment.links[i];
            if (link.method === 'REDIRECT') {
              redirectUrl = link.href;
            }
          }
          return res.redirect(redirectUrl);
        }
      };
    });
  });
});

router.get('/execute', function onExecute(req, res) {
  var paymentId = req.session.paymentId;
  var payerId = req.param('PayerID');
  var details = {
    "payer_id": payerId
  };
  paypal.payment.execute(paymentId, details, function (error, payment) {
    if (error) {
      console.log(error);
      return res.redirect('/signup?error=execute2');
    } else {
      console.log(payment);

      // Set status after completed
      User.findOne({
        paymentId: paymentId
      }, function (err, user) {
        if (err) {
          console.log(err);
          return res.redirect('/signup?error=execute1');
        };

        if (user) {
          user.status = 'paid';
          user.dueDate = new Date();
          if (user.term === 'monthly') {
            user.dueDate.setDate(user.dueDate.getDate() + 30);
          } else if (user.term === 'yearly') {
            user.dueDate.setDate(user.dueDate.getDate() + 365);
          };

          user.save();
        };
      });
      return res.render('thankyou', {
        title: "Thank You"
      });
    };
  });
});

router.get('/cancel', function onCancel(req, res) {
  //res.send("The payment got canceled");
  res.render('cancel', {
    title: "Too Bad"
  });
});

/* GET home page. */
router.get('/', function HomePage(req, res) {
  var url = req.get('host');
  console.log(req.headers);
  console.log('Got URL: ' + url);
  if ('localhost:3000' === url || 'nozama.io' === url) {
    res.render('home', {
      title: 'Nozama.io for Amazon Affiliates',
    });
  } else {
    console.log('Got URL: ' + url);
    Website.findOne({
      url: url
    }).populate('logo').exec(function (err, website) {

      if (err) {
        console.log(err);
      };

      if (website) {
        var template = 'default';
        Keyword.find({
          userId: website.userId
        }, function (err, keywords) {
          if (err) {
            console.log(err);
          };

          var logo = '/images/logo.png';
          if (website.logo) {
            logo = website.logo.url;
          };
          return res.render('templates/' + template + '/index', {
            title: website.title,
            description: website.description,
            logo: logo,
            keywords: keywords,
          });
        });


      } else {
        return res.render('home', {
          title: 'Nozama.io for Amazon Affiliates',
        });
      };
    });
  };
});

router.get('/about-us', function onAboutUs(req, res) {
  var url = req.get('host');
  if ('localhost:3000' === url || 'nozama.io' === url) {
    return res.render('about-us', {
      title: 'About Us',
    });
  } else {
    var template = 'default';

    Website.findOne({
      url: url
    }).populate('logo').exec(function (err, website) {
      if (err) {
        console.log(err);
      };

      return res.render('templates/' + template + '/about-us', {
        title: 'About Us',
        website: website,
        logo: website.logo.url
      });
    });

  };
});

router.get('/contact-us', function onContactUs(req, res) {

  var url = req.get('host');
  if ('localhost:3000' === url || 'nozama.io' === url) {
    return res.render('contact-us', {
      title: 'Contact Us',
    });
  } else {
    var template = 'default';
    return res.render('templates/' + template + '/contact-us', {
      title: 'Contact Us',
    });
  };
});

router.get('/pricing', function onPricing(req, res) {
  res.render('pricing', {
    title: 'Pricing',
  });
});

router.get('/signup', function onSignup(req, res) {
  var error = req.query.error;
  if (error === undefined || typeof (error) === 'undefined') {
    error = '';
  };
  res.render('signup', {
    title: 'Sign Up',
    error: error
  });
});

router.get('/login', function onLogin(req, res) {
  res.render('login', {
    title: 'Login',
  });
});

router.post('/login', function onPostLogin(req, res) {
  var username = req.body.username;
  var password = req.body.password;

  User.findOne({
    username: username,
    password: password
  }, function onFound(err, user) {
    if (err) {
      console.log(err);
      return res.redirect('/login?error=database-login');
    };

    console.log(user);
    if (user && user.status === 'paid') {
      req.session.uid = user._id;
      req.session.p = user.package;
      //req.signedCookies.uid = user._id;
      //req.signedCookies.p = user.package;
      res.cookie('uid', user._id, {
        signed: true
      });
      res.cookie('p', user.package, {
        signed: true
      });
      return res.redirect('/admin');
    } else if (user && user.username === 'admin' && user.status === 'admin') {
      req.session.uid = user._id;
      req.session.p = user.package;
      res.cookie('uid', user._id, {
        signed: true
      });
      res.cookie('p', user.package, {
        signed: true
      });
      return res.redirect('/power');
    } else {
      return res.redirect('/login?error=login');
    };
  });
});

router.get('/forget', function onForget(req, res) {
  res.render('forget', {
    title: 'Forget Password',
  });
});


var nodemailer = require('nodemailer');
var transporter = nodemailer.createTransport({
  service: 'Gmail',
  auth: {
    user: 'support@nozama.io',
    pass: 'BB1g0e2t5'
  }
});
router.post('/forget', function onPostForget(req, res) {
  var email = req.body.email;


  User.findOne({
    email: email
  }, function (err, user) {
    if (err) {
      console.log(err);
      return res.render('forget', {
        title: 'Forget Password',
        sent: true
      });
    };

    if (user) {
      // Send email
      var mailOptions = {
        from: 'Joe Zhou <support@nozama.io>', // sender address
        to: email,
        subject: 'Forget Password', // Subject line
        text: 'Your password is ' + user.password, // plaintext body
      };

      // send mail with defined transport object
      transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
          console.log(error);
        } else {
          console.log('Message sent: ' + info.response);

        };
        return res.render('forget', {
          title: 'Forget Password',
          sent: true
        });
      });


    } else {
      return res.render('forget', {
        title: 'Forget Password',
        sent: true
      });
    }
  });
});


router.get('/tos', function onForget(req, res) {
  var url = req.get('host');
  if ('localhost:3000' === url || 'nozama.io' === url) {
    return res.render('tos', {
      title: 'Term of Service',
    });
  } else {
    var template = 'default';
    return res.render('templates/' + template + '/tos', {
      title: 'Term of Service',
    });
  };

});

router.get('/privacy', function onForget(req, res) {
  var url = req.get('host');
  if ('localhost:3000' === url || 'nozama.io' === url) {
    return res.render('privacy', {
      title: 'Privacy Policy',
    });
  } else {
    var template = 'default';
    return res.render('templates/' + template + '/privacy', {
      title: 'Privacy Policy',
    });
  };
});

router.get('/logout', function onLogout(req, res) {
  req.session.uid = '';
  req.session.p = '';
  res.clearCookie('uid');
  res.clearCookie('p');

  res.redirect('/login');
});


// For client
router.get('/admin', function onAdmin(req, res) {
  helper.validateSession(req, function (err, user) {
    if (err) {
      console.log(err);
      return res.redirect('/login?error=' + err.status);
    };

    var fileDir = path.resolve(__dirname + '/../public/index2.html');
    if (user.status === 'unpaid') {
      fileDir = path.resolve(__dirname + '/../public/unpaid.html');
    } else if (user.package === 'premium') {
      fileDir = path.resolve(__dirname + '/../public/premium.html');
    };

    return res.sendFile(fileDir);
  });

});

// For admin
router.get('/power', function onPowerAdmin(req, res) {
  helper.validateSession(req, function (err, user) {
    if (err || user.username !== 'admin') {
      if (err) {
        console.log(err);
        return res.redirect('/login?error=' + err.status);
      } else {
        return res.redirect('/login?error=unauthorized');
      };
    };
    var fileDir = path.resolve(__dirname + '/../public/power.html');
    return res.sendFile(fileDir);
  });
});

router.get('/search', function onSearch(req, res) {
  console.log(req.query.q);
  Product.find({
    title: {
      $regex: req.query.q,
      $options: 'g'
    }
  }).sort('-permalink').limit(10).exec(function onList(err, products) {
    if (err) {
      console.log(err);
      return;
    };

    res.render('index', {
      title: 'Express',
      products: products
    });
  });
});

router.get('/contact', function (req, res) {
  res.render('contact', {
    test: 'Hello World'
  });
});

router.get('/new-releases', function onNewProduct(req, res) {
  var url = req.get('host');
  if ('localhost:3000' === url || 'nozama.io' === url) {
    res.render('home', {
      title: 'Nozama.io for Amazon Affiliates',
    });
  } else {
    Website.findOne({
      url: url
    }, function (err, website) {

      if (err) {
        console.log(err);
      };

      if (website) {
        var template = 'default';
        Keyword.find({
          userId: website.userId
        }).sort('-created').exec(function (err, keywords) {
          if (err) {
            console.log(err);
          };
          return res.render('templates/' + template +
            '/new-releases', {
              title: website.title + ' New Releases',
              description: website.description,
              keywords: keywords,
            });
        });


      } else {
        return res.render('home', {
          title: 'Nozama.io for Amazon Affiliates',
        });
      };
    });
  };
});

router.get('/recent-update', function onNewProduct(req, res) {
  var url = req.get('host');
  if ('localhost:3000' === url || 'nozama.io' === url) {
    res.render('home', {
      title: 'Nozama.io for Amazon Affiliates',
    });
  } else {
    Website.findOne({
      url: url
    }, function (err, website) {

      if (err) {
        console.log(err);
      };

      if (website) {
        var template = 'default';
        Keyword.find({
          userId: website.userId
        }).sort('-updated').exec(function (err, keywords) {
          if (err) {
            console.log(err);
          };
          return res.render('templates/' + template +
            '/new-releases', {
              title: website.title + ' Recent Update',
              description: website.description,
              keywords: keywords,
            });
        });


      } else {
        return res.render('home', {
          title: 'Nozama.io for Amazon Affiliates',
        });
      };
    });
  };
});



var zlib = require('zlib');
router.get('/sitemap.xml', function onSitemap(req, res) {
  var url = req.get('host');
  helper.generateSitemapXML(url, function (err, xml) {
    if (err) {
      return res.send('err');
    };

    res.header('Content-Type', 'text/xml');
    return res.send(xml);
  });
});

router.get('/sitemap.xml.gz', function onSitemap(req, res) {
  var url = req.get('host');
  helper.generateSitemapXML(url, function (err, xml) {
    if (err) {
      return res.send('err');
    };

    res.header('Content-Type: application/x-gzip');
    res.header('Content-Encoding: gzip');
    res.header(
      'Content-Disposition: attachment; filename="sitemap.xml.gz"');
    zlib.gzip(new Buffer(xml, 'utf8'), function (error, data) {
      if (error) {
        return res.send('err');
      };
      return res.send(data);
    });
  });
});

// For nozama article
router.get('/top-read', function (req, res) {

  redisClient.zrevrange('topread', 0, 9, 'withscores', function (err, replies) {
    if (err) {
      console.log(err);
    };

    var ids = [];
    var views = [];
    // Get in array for query
    for (var i = 0; i < replies.length - 1; i += 2) {
      ids.push(replies[i]);
      views.push(replies[i + 1]);
    };

    Article.find({
      _id: {
        $in: ids
      }
    }).lean().exec(function (err, articles) {
      if (err) {
        console.log(err);
        return;
      };

      // Get views
      for (var i = articles.length - 1; i >= 0; i--) {
        articles[i].read = views[ids.indexOf(articles[i]._id.toString())];
      };

      articles.sort(function (a, b) {
        return b.read - a.read;
      });
      return res.render('top-read', {
        title: 'Top Read on Nozama.io',
        articles: articles
      });
    })
  });

});

router.get('/recent-articles', function (req, res) {

  var m_names = new Array("Jan", "Feb", "Mar",
    "Apr", "May", "Jun", "Jul", "Aug", "Sep",
    "Oct", "Nov", "Dec");


  Article.find({
    content: {
      $exists: true
    }
  }).sort('-updated').limit(9).lean().exec(function (err, articles) {
    if (err) {
      console.log(err);
      return;
    };

    for (var i = articles.length - 1; i >= 0; i--) {
      var d = articles[i].updated;
      var curr_date = d.getDate();
      var curr_month = d.getMonth();
      var curr_year = d.getFullYear();
      articles[i].updated = curr_date + "-" + m_names[curr_month] + "-" + curr_year;
    };
    return res.render('recent-articles', {
      title: 'Top Read on Nozama.io',
      articles: articles
    });
  });

});

router.get('/:keyword', function onKeywordDetail(req, res) {
  var keyword = req.params.keyword;

  var url = req.get('host');
  if ('localhost:3000' === url || 'nozama.io' === url) {

    // Pivoted. We provide article.
    Article.findOne({
      permalink: keyword
    }, function (err, article) {
      if (err) {
        console.log(err);
        return res.render('home', {
          title: 'Nozama.io for Amazon Affiliates',
        });
      };

      Article.find({
        cat: article.cat
      }).lean().limit(3).exec(function (err, articles) {
        return res.render('article', {
          title: article.title,
          description: article.summary,
          article: article,
          articles: articles
        });
      });

    });

  } else {

    helper.getWebsite(url, function (err, website) {
      if (err) {
        console.log(err);
        return res.render('home', {
          title: 'Nozama.io for Amazon Affiliates',
        });
      };

      var template = 'default';
      Keyword.findOne({
        permalink: keyword,
        userId: website.userId
      }, function (err, keyword) {
        if (err) {
          console.log(err);
          return res.render('home', {
            title: 'Nozama.io for Amazon Affiliates',
          });
        };
        if (keyword) {

          Keyword.find({
              _id: {
                '$ne': keyword._id
              }
            })
            .limit(3).exec(function (err, keywords) {
              if (err) {
                console.log(err);
                return res.render('templates/' + template +
                  '/detail', {
                    title: keyword.title,
                    description: keyword.description,
                    keyword: keyword,
                    keywords: [],
                  });
              };

              return res.render('templates/' + template +
                '/detail', {
                  title: keyword.title,
                  description: keyword.description,
                  keyword: keyword,
                  keywords: keywords,
                });
            })

        } else {
          return res.render('home', {
            title: 'Nozama.io for Amazon Affiliates',
          });
        };

      });
    });
  };
  return;
});

router.get('/go/:keyword', function (req, res) {
  var keyword = req.params.keyword;
  var url = req.get('host');
  Keyword.findOne({
    permalink: keyword,
  }, function (err, keyword) {
    if (err) {
      console.log(err);
    };
    if (keyword) {
      return res.redirect('http://www.amazon.com/dp/' + keyword.asin +
        '?tag=' + keyword.trackingId);
    } else {
      return res.redirect('http://' + url);
    };

  });
});





// For hook
router.post('/githook', function (req, res) {
  var exec = require('child_process').exec;
  exec("./hook.sh", function (err, stdout, stderr) {
    if (err) {
      console.log(stderr);
    } else {
      console.log('Hook successful');
    };

    res.send('OK');
  });
});



module.exports = router;

module.exports.init = function (c) {
  config = c;
  paypal.configure(c.api);
};
