var express = require('express');

var router = express.Router();

var request = require('request');
var cheerio = require('cheerio');

var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;
var Article = require('../models/article');
var Device = require('../models/device');
var kue = require('kue');
var jobs = kue.createQueue();

var redis = require("redis");
var redisClient = redis.createClient();

router.get('/grab', function (req, res) {
  var url = 'http://ezinearticles.com/?cat=Business';
  request({
    url: url
  }, function onRequest(error, response, html) {
    if (!error) {
      var $ = cheerio.load(html);
      var articles = [];
      $('.article').filter(function () {
        var article = {};
        article.title = $('.article-title-link', this).text();
        article.permalink = article.title.replace(/[^a-zA-Z0-9 ]/g, '');
        article.permalink = article.permalink.replace(/ /g, '-');
        article.link = $('.article-title-link', this).attr('href');
        article.by = $('.article-byline', this).text();
        article.summary = $('.article-summary', this).text();
        articles.push(article);
        // var article = {};
        // var data = $(this);
        // data.filter('.article-title-link', function() {
        //   article.title = $(this).text();
        // });

        //
      });

      res.send(articles);
    } else {
      console.log(error);
      console.log('error');
      res.send('error');
    }
  });

});

router.get('/detail', function (req, res) {
  var url = 'http://ezinearticles.com/?Starting-On-The-Right-Foot:-Your-Business-in-2015&id=8887256';
  //var url = 'http://ezinearticles.com/?Precision-in-Selecting-the-Right-Harmonized-Code-Is-Essential-for-Importers-and-Exporters&id=8883782';
  request({
    url: url
  }, function onRequest(error, response, html) {
    if (!error) {
      var $ = cheerio.load(html);
      var articles = [];
      var article = {};

      // Grab content
      $('#article-content').filter(function () {
        console.log('heyhey');
        article.content = $(this).text().replace('(adsbygoogle = window.adsbygoogle || []).push({});', '');
      });

      // grab ezine id
      var regex = /(.*)&id=(.*)/g;
      var result = regex.exec(url);
      if (result && result.length > 0) {
        article.eid = result[2];
      };

      article.resource = $('#article-resource').html();

      regex = /Article Source:(.*)<\/p>/g;
      regex.exec($('#article-body').html());
      if (result && result.length > 0) {
        article.source = result[1];
      };

      articles.push(article);
      res.send(articles);
    } else {
      console.log(error);
      console.log('error');
      res.send('error');
    }
  });

});

router.get('/test', function (req, res) {
  var url = 'http://ezinearticles.com/?Starting-On-The-Right-Foot:-Your-Business-in-2015&id=8887256';
  regex = /(.*)&id=(.*)/g;
  var result = regex.exec(url);
  if (result && result.length > 0) {
    //keyword.star = result[1].trim();
    console.log(result[2]);
  };

  regex = /Article Source:(.*)<\/p>/g;
  var str = '<div id="article-resource"><p>Ben Black is the Director of <a target="_blank" rel="nofollow" href="http://www.calculatedaccountants.co.uk">Calculated Accountants</a>, a firm of professional start up business and small business accountants dedicated to providing solid, trusted financial advice and support.</p>     </div><p>Article Source:<a href="/?expert=B_Black">http://EzineArticles.com/?expert=B_Black</a></p></div>';
  result = regex.exec(str);
  if (result && result.length > 0) {
    console.log(result[1]);
  };

  res.send('ok');
});

router.get('/links', function (req, res) {
  var cat = req.query.cat || 'Gaming:Video-Game-Reviews';
  var url = 'http://ezinearticles.com/?cat=' + cat;

  var milliseconds = Math.floor((Math.random() * 3000) + 500);
  var page = 1;
  for (var i = 1; i <= 1067; i++) {
    jobs.create('getEzineLinks', {
      title: 'Get all links from ' + url,
      url: url,
      page: i
    }).delay(milliseconds).save(function onJobCreated(err) {
      if (err) {
        console.log(err);
        return callback(err);
      };
    });
    milliseconds += Math.floor((Math.random() * 3000) + 500);
  };


  res.send('ok ' + cat);
});

jobs.process('getEzineLinks', function onProcessGetReviews(job, done) {

  var url = job.data.url;
  var page = job.data.page;
  var getUrl = url + '&page=' + page;
  console.log(getUrl);

  request({
    url: getUrl
  }, function onRequest(error, response, html) {
    if (!error) {
      var $ = cheerio.load(html);
      $('.article').filter(function () {
        var article = {};

        article.link = $('.article-title-link', this).attr('href');
        regex = /(.*)&id=(.*)/g;
        var result = regex.exec(article.link);
        if (result && result.length > 0) {
          article.eid = result[2];
        } else {
          console.log('cannot find eid');
          return;
        }

        article.title = $('.article-title-link', this).text();
        article.permalink = article.title.replace(/[^a-zA-Z0-9 ]/g, '');
        article.permalink = article.permalink.replace(/ /g, '-');

        article.by = $('.article-byline', this).text();
        article.summary = $('.article-summary', this).text();

        Article.findOne({
          eid: article.eid
        }, function (err, existingArticle) {
          if (err) {
            console.log(err);
            return;
          };

          if (existingArticle) {
            // Do nothing, maybe update
          } else {
            // create new one
            var newArticle = new Article(article);
            newArticle.save();

            // Should create a get detail job
          };
        });
      });

      done();
    } else {
      console.log(error);
      console.log('error');
      //res.send('error');
      done();
    };
  });
});

router.get('/details', function (req, res) {
  // var url = 'http://ezinearticles.com/?cat=Business';
  Article.find({
    content: {
      $exists: false
    }
  }).limit(10).exec(function (err, articles) {
    if (err) {
      console.log(err);
      return res.send('error');
    };
    var milliseconds = Math.floor((Math.random() * 15000) + 15000);
    var page = 1;

    for (var i = 0; i < articles.length; i++) {
      var url = 'http://ezinearticles.com/?id=' + articles[i].eid;
      var eid = articles[i].eid;
      jobs.create('getEzineDetails', {
        title: 'Get article detail from ' + url,
        url: url,
        eid: eid
      }).delay(milliseconds).save(function onJobCreated(err) {
        if (err) {
          console.log(err);
          return callback(err);
        };
      });
      milliseconds += Math.floor((Math.random() * 15000) + 15000);
    };
    return res.send('ok');
  });
});

jobs.process('getEzineDetails', function onProcessGetReviews(job, done) {
  var url = job.data.url;
  var eid = job.data.eid;

  Article.findOne({
    eid: eid
  }, function (err, existingArticle) {
    if (err) {
      console.log(err);
      return done();
    };

    if (!existingArticle) {
      console.log('not exists');
      return done();
    };

    request({
      url: url
    }, function onRequest(error, response, html) {
      if (!error) {
        var $ = cheerio.load(html);
        var article = {};

        // Grab content
        $('#article-content').filter(function () {
          article.content = $(this).text().replace('(adsbygoogle = window.adsbygoogle || []).push({});', '');
        });

        // grab ezine id
        var regex = /(.*)&id=(.*)/g;
        var result = regex.exec(url);
        if (result && result.length > 0) {
          article.eid = result[2];
        };

        article.resource = $('#article-resource').html();

        regex = /Article Source:(.*)<\/p>/g;
        regex.exec($('#article-body').html());
        if (result && result.length > 0) {
          article.source = result[1];
        };

        existingArticle.content = article.content;
        existingArticle.source = article.source;
        existingArticle.resource = article.resource;
        existingArticle.updated = new Date();
        existingArticle.markModified('content');
        existingArticle.markModified('source');
        existingArticle.markModified('resource');
        existingArticle.markModified('updated');
        existingArticle.save();
        done();
      } else {
        console.log(error);
        console.log('error');
        //res.send('error');
        done();
      };
    });
  });
});

jobs.process('getEzineDetail', function onProcessGetReviews(job, done) {
  var url = job.data.url;
  var eid = job.data.eid;

  console.log('Get content for eid:' + eid);

  Article.findOne({
    eid: eid
  }, function (err, existingArticle) {
    if (err) {
      console.log(err);
      return done();
    };

    if (!existingArticle) {
      console.log('not exists');
      return done();
    };

    request({
      url: url
    }, function onRequest(error, response, html) {
      if (!error) {
        var $ = cheerio.load(html);
        var article = {};

        // Grab content
        $('#article-content').filter(function () {
          article.content = $(this).text().replace('(adsbygoogle = window.adsbygoogle || []).push({});', '');
        });

        // grab ezine id
        var regex = /(.*)&id=(.*)/g;
        var result = regex.exec(url);
        if (result && result.length > 0) {
          article.eid = result[2];
        };

        article.resource = $('#article-resource').html();

        regex = /Article Source:(.*)<\/p>/g;
        regex.exec($('#article-body').html());
        if (result && result.length > 0) {
          article.source = result[1];
        };

        existingArticle.content = article.content;
        existingArticle.source = article.source;
        existingArticle.resource = article.resource;
        existingArticle.updated = new Date();
        existingArticle.markModified('content');
        existingArticle.markModified('source');
        existingArticle.markModified('resource');
        existingArticle.markModified('updated');
        existingArticle.save(function (err) {
          if (err) {
            console.log(err);
            return;
          };

          // send noti
          sendNotiByCat(existingArticle.cat, function (err) {
            if (err) {
              console.log(err);
            };
          });

          done();
        });

      } else {
        console.log(error);
        console.log('error');
        //res.send('error');
        done();
      };
    });
  });
});



/**
 * API
 */

router.get('/articles', function (req, res) {
  var cat = req.query.cat || 'business';
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Credentials', true);
  res.header('Access-Control-Allow-Methods', 'POST, GET');
  res.header('Access-Control-Allow-Headers', 'Content-Type');

  var page = 1;
  if (req.query.page !== undefined) {
    page = req.query.page;
  };

  if (page == 1) {
    Article.find({
      cat: cat,
      content: {
        $exists: true
      },
    }).sort('-updated').limit(10).exec(function (err, articles) {
      if (err) {
        console.log(err);
        return res.send([]);
      };

      return res.send(articles);
    });
  } else {
    var skip = (parseInt(page) - 1) * 10;
    console.log(skip);
    Article.find({
      cat: cat,
      content: {
        $exists: true
      },
    }).sort('-updated').skip(skip).limit(10).exec(function (err, articles) {
      if (err) {
        console.log(err);
        return res.send([]);
      };
      return res.send(articles);
    });
  };


});

router.get('/articles/:articleId', function (req, res) {
  var articleId = req.params.articleId;
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Credentials', true);
  res.header('Access-Control-Allow-Methods', 'POST, GET');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  Article.findOne({
    _id: articleId
  }, function (err, article) {
    if (err) {
      console.log(err);
      return res.send({});
    };

    return res.send(article);
  });
});

router.get('/search', function (req, res) {
  var keyword = req.query.keyword;
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Credentials', true);
  res.header('Access-Control-Allow-Methods', 'POST, GET');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  Article.find({
    title: {
      $regex: keyword,
      $options: 'g'
    }
  }).exec(function (err, articles) {
    if (err) {
      console.log(err);
      return res.send([]);
    };

    return res.send(articles);
  });
});

var gcm = require('node-gcm');

router.post('/subscribe', function (req, res) {
  var regid = req.body.regid;
  var type = req.body.type;
  var cat = req.body.cat;
  Device.findOne({
    regid: regid
  }, function (err, device) {
    if (err) {
      console.log(err);
      return res.send('error');
    };

    if (device) {
      // Nothing
      return res.send('ok');
    } else {
      var newDevice = new Device(req.body);
      newDevice.save(function (err) {
        if (err) {
          console.log(err);
          return res.send('error');
        };
        return res.send('ok');
      })
    };
  });

});

var sender = new gcm.Sender('AIzaSyDYCYqsZGKVtB9f8WeiK4o2R_xEuIuxNhQ');
router.get('/testsend', function (req, res) {
  var message = new gcm.Message();
  message.addData('message', 'Hello Cordova!');
  message.addData('title', 'Push Notification Test');
  message.addData('msgcnt', '2'); // Shows up in the notification in the status bar
  message.collapseKey = 'demo';
  message.delayWhileIdle = true;
  message.timeToLive = 4;
  //message.dryRun = true;

  Device.find({}, function (err, devices) {
    if (err) {
      console.log(err);
      return res.send('error');
    };
    var registrationIds = [];
    for (var i = devices.length - 1; i >= 0; i--) {
      registrationIds.push(devices[i].regid);
    };
    sender.send(message, registrationIds, 4, function (err, result) {
      if (err) {
        console.log(err);
        return res.send('error');
      };
      console.log(result);
      return res.send('ok');
    });
  });
});

router.get('/grabcat', function (req, res) {
  if (req.query.cat !== undefined && req.query.cat !== '') {
    Article.findOne({
      cat: req.query.cat,
      content: {
        $exists: false
      }
    }).exec(function (err, article) {
      if (err) {
        console.log(err);
        return res.send('error');
      };

      var url = 'http://ezinearticles.com/?id=' + article.eid;
      var eid = article.eid;
      jobs.create('getEzineDetail', {
        title: 'Get article detail from ' + url,
        url: url,
        eid: eid
      }).save(function onJobCreated(err) {
        if (err) {
          console.log(err);
          return res.send('error');
        };
      });
      return res.send('ok');
    });
  } else {
    return res.send('error');
  };
});

router.get('/updatefield', function (req, res) {
  Article.find({
    ucat: {
      $exists: false
    }
  }, function (err, articles) {
    if (err) {
      console.log(err);
      return res.send(err);
    };
    for (var i = articles.length - 1; i >= 0; i--) {
      articles[i].cat = 'business';
      articles[i].updated = new Date();
      articles[i].save();
    };

    return res.send('ok');
  });
});

var sendNotiByCat = function (cat, done) {
  var message = new gcm.Message();
  message.addData('message', 'New article is waiting for you!');
  message.addData('title', 'Article Update!');
  message.addData('msgcnt', '1'); // Shows up in the notification in the status bar
  Device.find({
    cat: cat
  }, function (err, devices) {
    if (err) {
      console.log(err);
      return done(err);
    };
    var registrationIds = [];
    for (var i = devices.length - 1; i >= 0; i--) {
      registrationIds.push(devices[i].regid);
    };
    sender.send(message, registrationIds, 4, function (err, result) {
      if (err) {
        console.log(err);
        return done(err);
      };
      console.log(result);
      return done(null);
    });
    done(null);
  });
};


// Stat
router.get('/read', function (req, res) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Credentials', true);
  res.header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  redisClient.zincrby('topread', 1, req.query.eid);
  redisClient.sadd(req.query.uuid, req.query.eid);
  return res.send('ok');
});

router.get('/topread', function (req, res) {
  redisClient.zrevrangebyscore('topread', '+inf', '-inf', function (err, reply) {
    if (err) {
      console.log(err);
    };
    return res.send(reply);
  })
});

module.exports = router;
