// Make Money $1000,000,000.00

var express = require('express');

var request = require('request');
var cheerio = require('cheerio');
var mongoose = require('mongoose');
var kue = require('kue');

var Product = require('../models/product');
var Pr = require('../models/pr');
var Domain = require('../models/domain');
var Review = require('../models/review');
var Post = require('../models/post');
var Keyword = require('../models/keyword');
var KeywordCache = require('../models/keywordCache');

var helper = require('./helper');

var htmlparser = require("htmlparser2");
var wordpress = require("wordpress");


var router = express.Router();

// Creates new queue
var jobs = kue.createQueue();

// For PR
var PageRank = require('pagerank');


/**
 * Get basic data from Amazon
 */
router.get('/', function (req, res) {
  // Bestseller upcoming
  var url =
    'http://www.amazon.com/s/ref=s9_acsd_adnr_bw_nr_r0_c1?_encoding' +
    '=UTF8&field-releasedate=7x-150y&node=468642&rank=salesrank&pf_rd_m=' +
    'ATVPDKIKX0DER&pf_rd_s=merchandised-search-3&pf_rd_r=0KN64AGR8EJ7YJRAKYN9&' +
    'pf_rd_t=101&pf_rd_p=1926391682&pf_rd_i=496994';

  helper.getNewReleases(url, function onComplete(err) {
    if (err) {
      console.log(err);
      res.send(err);
      return;
    };
    return res.send('ok');
  });
});

router.get('/detail', function onCreateDetailJobs(req, res) {
  Product.find({}, {
    asin: 1
  }).exec(function onProducts(err, products) {
    console.log('Create ' + products.length + ' jobs');
    for (var i = products.length - 1; i >= 0; i--) {
      var asin = products[i].asin;
      jobs.create('getProductDetail', {
        title: 'Get Product Detail on' + asin,
        asin: asin
      }).save(function onJobCreated(err) {
        if (err) {
          console.log(err);
          return;
        }
      });
    };
    return res.send(products);
  });
});

/**
 * Show basic list
 */
router.get('/list', function onRouteList(req, res) {
  Product.find({}).sort('rank').limit(10).exec(function onList(err,
    products) {
    if (err) {
      console.log(err);
      return;
    };

    return res.render('list', {
      products: products
    });
  });
});

router.get('/asin', function onGetASIN(req, res) {
  var asin = 'B00AWMP876';
  var url = 'http://www.amazon.com/dp/B00AWMP876';


  request(url, function (error, response, html) {
    if (!error && response.statusCode == 200) {


      var keyword = {};
      var key = '';
      var parser = new htmlparser.Parser({
        onopentag: function (name, attribs) {
          // console.log(attribs);
          if (name === 'h1' && attribs.class ===
            'parseasinTitle ') {
            key = 'title';
          } else if (name === 'span') {
            if (attribs.id) {
              console.log(attribs);
            };
          };
        },
        ontext: function (text) {
          //console.log("-->", text);
          if (key !== '' && text.trim() !== '') {
            //console.log(key);
            // console.log('Text: ' + text);
            // console.log('Type: ' + typeof(text));
            if (keyword[key] === undefined) {
              keyword[key] = text;
            } else {
              keyword[key] += text;
            };

          };
        },
        onclosetag: function (tagname) {
          if (key !== '') {
            console.log("That's it?!");
            keyword[key] = keyword[key].trim();
            key = '';
          };
        }
      });

      parser.write(html);
      parser.end();
      res.send(keyword);
    } else {
      res.send('not ok');
    };
  });
  // helper.getAmazonUrl(url, function (html) {
  //   var $ = cheerio.load(html);
  //   var keyword = {};
  //   keyword.asin = asin;
  //   //keyword.imageUrl = $('#imgTagWrapperId').attr('src');
  //   $('h1#title').filter(function() {
  //     console.log($(this).text());
  //   });

  //   $('#leftCol').filter(function() {
  //     console.log('left');
  //   });

  //   $('#centerCol').filter(function() {
  //     console.log('center');
  //   });
  //   $('#imageBlock_feature_div').filter(function() {
  //     console.log('yoyo');
  //     var regex = /var data = (.*);/g;
  //     var result = regex.exec($(this).html());
  //     if (result && result.length > 0) {
  //       console.log(result);
  //     };
  //   });
  //   console.log(keyword);
  //   res.send('ok');
  // });

  // for game
  // helper.getProductDetail(asin, function onDetail(err, aProduct) {
  //   if (err) {
  //     console.log(err);
  //     return;
  //   };

  //   'colorImages':

  //   Product.findOne({
  //     asin: asin
  //   }, function onFound(err, product) {
  //     if (err) {
  //       console.log(err);
  //       return;
  //     };

  //     product.title = aProduct.title;
  //     product.rank = aProduct.rank;
  //     product.description = aProduct.description;
  //     product.images = aProduct.images;
  //     product.save(function onSaved(err) {
  //       if (err) {
  //         console.log(err);
  //         return;
  //       };
  //       return res.send(aProduct);
  //     });
  //   });
  // });
});

router.get('/google', function onGetGoogleResult(req, res) {
  Product.find({}, {
    asin: 1,
    title: 1
  }).exec(function onProducts(err, products) {

    if (err) {
      console.log(err)
      return;
    };

    console.log('Research ' + products.length + ' keywords');
    var milliseconds = Math.floor((Math.random() * 5000) + 3000);
    for (var i = products.length - 1; i >= 0; i--) {
      var keyword = products[i].title;
      var asin = products[i].asin;

      // Clean up keyword
      keyword = keyword.replace(/\((.*)\)/g, '');
      keyword = keyword.replace(/\[(.*)\]/g, '');
      keyword = keyword.replace(/- /g, '');
      keyword = keyword.replace(/\s+/g, ' ');
      milliseconds += Math.floor((Math.random() * 5000) + 3000);
      jobs.create('getGoogleResults', {
        title: 'Research Keyword: ' + asin + ' on Google',
        asin: asin,
        keyword: keyword,
        matching: 'broad'
      }).delay(milliseconds).save(function onJobCreated(err) {
        if (err) {
          console.log(err);
          return;
        }
      });

      keyword = '%22' + keyword + '%22';
      milliseconds += Math.floor((Math.random() * 5000) + 3000);
      jobs.create('getGoogleResults', {
        title: 'Research Phrase Keyword: ' + asin + ' on Google',
        asin: asin,
        keyword: keyword,
        matching: 'phrase'
      }).delay(milliseconds).save(function onJobCreated(err) {
        if (err) {
          console.log(err);
          return;
        }
      });

    };
    return res.send(products);
  });
  return;
});


router.get('/category', function (req, res) {

  // helper.getMainCategories(function (categories) {
  //   res.send(categories);
  // });
  var url =
    'http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Dbaby-products';
  helper.getSubcategories(url, function onGetSubcategories(err, categories) {
    if (err) {
      console.log(err);
      return;
    };
    return res.send(categories);
  });
});

router.get('/keywords', function onGetKeywords(req, res) {
  var url =
    'http://www.amazon.com/s/ref=lp_166742011_nr_n_0/189-4025316-3627361?rh=n%3A165796011%2Cn%3A%21165797011%2Cn%3A695338011%2Cn%3A166742011%2Cn%3A166743011&bbn=166742011&ie=UTF8&qid=1415162047&rnid=166742011';

  helper.getKeywordsFromUrl(url, function (keywords) {
    return res.send(keywords);
  });
});

router.get('/test', function (req, res) {
  var url =
    'http://www.amazon.com/Convertible-Car-Seats/b/ref=Baby_0714_HP_Left2_Leftnav_CarSeats_Convertibles/190-0072541-2086537?ie=UTF8&node=166839011&pf_rd_m=ATVPDKIKX0DER&pf_rd_s=merchandised-search-left-2&pf_rd_r=0ANN0BYJ0V4VQ3F707Y6&pf_rd_t=101&pf_rd_p=1952581982&pf_rd_i=165796011';

  var regex = /&node=(\d+)&/;
  var result = regex.exec(url);
  var node = '';
  if (result && result.length > 0) {
    var node = result[1];
    console.log(result);
  } else {};
  console.log(node);
  res.send(url);
});


// PR Saga
router.get('/prlist', function (req, res) {

  var domains = '';
  Pr.find({}).sort('-pr').limit(100).exec(function (err, prs) {
    for (var i = 0; i < prs.length; i++) {
      domains += prs[i].url + ' ' + prs[i].pr + '\n<br>';
    };
    res.send(domains);
  });
});
router.get('/pr', function (req, res) {
  //alt1.toolbarqueries.google.

  //var PageRank = require('pagerank');
  //var web = new PageRank('http://stylesaccessories.com');
  //web.on('data', console.log).on('error', console.error);

  //var url = 'http://w4rfd.com';

  var fs = require('fs'),
    readline = require('readline');
  var rd = readline.createInterface({
    input: fs.createReadStream(__dirname + '/../url.txt'),
    output: process.stdout,
    terminal: false
  });

  var milliseconds = Math.floor((Math.random() * 3000) + 1000);
  rd.on('line', function (line) {
    var url = line;
    jobs.create('getPR', {
      title: 'Get PR for ' + url,
      url: url
    }).delay(milliseconds).save(function onJobCreated(err) {
      if (err) {
        console.log(err);
        return;
      }
    });
    milliseconds += Math.floor((Math.random() * 3000) + 1000);
  });
  res.send('ok');
});

jobs.process('getPR', function onPr(job, done) {
  //var PageRank = require('pagerank');
  console.log('Process pr for ' + job.data.url);
  var web = new PageRank();
  web.get(job.data.url, function (err, pr) {
    if (err) {
      console.log(err);
      return;
    };

    if (pr) {
      console.log(job.data.url + ' has pr ' + pr);
      var urlPr = new Pr();
      urlPr.url = job.data.url;
      urlPr.pr = pr;
      urlPr.save();
    };

    done();

  });
});

jobs.process('getPRforDomain', function onPr(job, done) {
  //var PageRank = require('pagerank');
  if (job.data.url === undefined) {
    return done();
  };
  console.log('Process pr for ' + job.data.url);
  Pr.findOne({
    url: job.data.url
  }, function (err, pr) {
    if (err) {
      console.log(err);
      return;
    };

    if (pr) {
      Domain.findOne({
        _id: job.data.domainId
      }, function (err, domain) {
        if (err) {
          console.log(err);
          return;
        };

        if (domain) {
          domain.pr = pr._id;
          domain.markModified('pr');
          domain.save();
          done();
        } else {
          // Fail silently
          console.log('No domain');
          done();
        }
      });
    } else {
      // No pr yet, fine one
      var web = new PageRank();
      web.get(job.data.url, function (err, pr) {
        if (err) {
          console.log(err);
          return;
        };

        if (pr) {
          console.log(job.data.url + ' has pr ' + pr);
          var urlPr = new Pr();
          urlPr.url = job.data.url;
          urlPr.pr = pr;
          urlPr.save(function (err) {
            if (err) {
              console.log(err);
              return;
            };
            Domain.findOne({
              _id: job.data.domainId
            }, function (err, domain) {
              if (err) {
                console.log(err);
                return;
              };

              if (domain) {
                domain.pr = urlPr._id;
                domain.markModified('pr');
                domain.save();
                done();
              } else {
                // Fail silently
                console.log('No domain');
                done();
              }
            });

          });
        } else {
          console.log('Cannot get pr');
        };

        done();

      });
    };
  });

});

// Reviews
router.get('/createpost', function (req, res) {
  KeywordCache.find({}, function (err, keywords) {
    if (err) {
      console.log(err);
      return res.send(err);
    };
  });
});
router.get('/createpost/:asin', function (req, res) {
  //var asin = 'B00AWMP876';
  var asin = req.params.asin;
  Keyword.findOne({
    asin: asin
  }, function (err, keyword) {
    if (err) {
      console.log(err);
      return res.send(err);
    };

    if (!keyword) {
      return res.send('no keyword');
    };
    Review.find({
      asin: asin
    }, function (err, reviews) {
      if (err) {
        console.log(err);
        return res.send(err);
      };

      console.log(keyword);
      var posts = [];
      for (var i = reviews.length - 2; i >= 1; i -= 2) {
        var post = new Post();
        post.title = keyword.title + ' Review';
        post.content = '<img src="' + keyword.imageUrl;
        post.content += '" style="float: left">';
        post.content += reviews[i].content + '<br>';
        post.content += reviews[i + 1].content;
        post.content += '<br>';
        post.content += '<a href="http://www.amazon.com/dp/' +
          keyword
          .asin;
        post.content += '?tag=' + keyword.trackindId + '">';
        post.content += 'Read more about ' + keyword.keyword +
          '</a>';
        post.asin = asin;
        post.save();
        posts.push(post);
      };

      res.render('post', {
        posts: posts
      });
    });
  });

});
router.get('/reviewlist', function (req, res) {
  Review.find({
    asin: 'B00AWMP876'
  }, function (err, reviews) {
    res.send(reviews);
  });
});
router.get('/reviews/:asin', function (req, res) {
  //var asin = 'B00AWMP876';
  var asin = req.params.asin;
  var url = 'http://www.amazon.com/product-reviews/' + asin;
  helper.getAmazonUrl(url, function (html) {
    var $ = cheerio.load(html);

    $('#productReviews').filter(function () {
      $('.reviewText').filter(function (index, element) {
        var review = new Review();
        review.asin = asin;
        review.content = $(this).text();
        review.save();
      });
    });

    var isLast = true;
    $('.paging').filter(function (index, element) {

      // Check only once
      if (index == 0) {
        isLast = false;
        $('a', this).filter(function (index, element) {
          if ($(this).text().trim() === 'Next ›') {
            // Get Next Page URL
            console.log($(this).attr('href'));

            // Create a new job here
            var url = $(this).attr('href');
            jobs.create('getNextPageReviews', {
              title: 'Get Reviews',
              url: url,
              asin: asin
            }).save(function onJobCreated(err) {
              if (err) {
                console.log(err);
                return;
              };
            });
          };
        });
      };


    });
    res.send('OK');

  });
});

router.get('/testpost', function onTestPost(req, res) {

  var client = wordpress.createClient({
    url: "sandbox.poomsoft.com",
    username: "admin",
    password: "passw0rd"
  });

  // client.getPosts(function(error, posts) {
  //   console.log("Found " + posts.length + " posts!");
  //   res.send('ok');
  // });
  var newPost = {
    name: "test-name",
    title: "Hello Nozama!",
    content: "test",
    status: "publish",
    tags_input: "test 1",
  };
  client.newPost(newPost, function (err, id) {
    console.log('Post completed:' + id);
    return;
  })
});

router.get('/post', function onPostJob(req, res) {

  var client = wordpress.createClient({
    url: "sandbox.poomsoft.com",
    username: "admin",
    password: "passw0rd"
  });
  Post.findOne({
    status: 'pending'
  }, function (err, post) {
    var newPost = {};
    newPost.title = post.title;
    newPost.content = post.content;
    newPost.status = 'publish';
    post.status = 'complete';
    post.markModified('status');
    post.save();
    client.newPost(newPost, function (err, id) {
      console.log('Post completed:' + id);
      return res.send('OK');
    });
  });
});

jobs.process('getNextPageReviews', function onProcessGetReviews(job, done) {


  var url = job.data.url;
  var asin = job.data.asin;
  var user = job.data.user;
  helper.getAmazonUrl(url, function (html) {
    var $ = cheerio.load(html);

    $('.reviewText').filter(function (index, element) {
      //console.log($(this).text());
      var review = new Review();
      review.asin = asin;
      review.content = $(this).text();
      review.save();
    });

    var isLast = true;
    $('.paging').filter(function (index, element) {

      // Check only once
      if (index == 0) {
        $('a', this).filter(function (index, element) {
          if ($(this).text().trim() === 'Next ›') {
            isLast = false;
            // Get Next Page URL
            //console.log($(this).attr('href'));

            // Create a new job here
            var url = $(this).attr('href');
            jobs.create('getNextPageReviews', {
              title: 'Get Reviews',
              url: url,
              asin: asin,
              user: user
            }).save(function onJobCreated(err) {
              if (err) {
                console.log(err);
                return;
              };
            });
          };
        });
      };
    });

    if (isLast) {
      jobs.create('createPostASIN', {
        title: 'Get ASIN',
        asin: asin,
        user: user
      }).save(function onJobCreated(err) {
        if (err) {
          console.log(err);
          return;
        };
      });
    } else {
      console.log('One more page');
    }
    done();
  });

});

jobs.process('createPostASIN', function onProcessGetReviews(job, done) {
  var asin = job.data.asin;
  var user = job.data.user;
  console.log(asin);
  Keyword.findOne({
    asin: asin,
    userId: user._id
  }, function (err, keyword) {
    if (err) {
      console.log(err);
      return res.send(err);
    };

    if (!keyword) {
      console.log('no keyword');
      return done();
    };
    Review.find({
      asin: asin
    }, function (err, reviews) {
      if (err) {
        console.log(err);
        return res.send(err);
      };

      //console.log(keyword);
      //console.log('Get ' + reviews.length + ' reviews');
      var posts = [];
      for (var i = reviews.length - 2; i >= 1; i -= 2) {
        var post = new Post();
        post.title = keyword.title + ' Review';
        post.content = '<img src="' + keyword.imageUrl;
        post.content += '" style="float: left">';
        post.content += reviews[i].content + '<br>';
        post.content += reviews[i + 1].content;
        post.content += '<br>';
        post.content += '<a href="http://www.amazon.com/dp/' +
          keyword
          .asin;
        post.content += '?tag=' + keyword.trackindId + '">';
        post.content += 'Read more about ' + keyword.keyword +
          '</a>';
        post.asin = asin;
        post.userId = user._id;
        post.save();
        posts.push(post);
      };

      console.log(posts.length + ' posts generated');

      done();
    });
  });
});

jobs.process('postToNetwork', function onPostToNetwork(job, done) {
  var client = wordpress.createClient({
    url: job.data.network.url,
    username: job.data.network.username,
    password: job.data.network.password
  });
  client.newPost(job.data.post, function (err, id) {
    if (err) {
      console.log(err);
    };
    console.log('Post completed:' + id);
    done();
  });
});

router.get('/testcron', function onTestCron(req, res) {
  var CronJob = require('cron').CronJob;
  var job = new CronJob({
    cronTime: '00 15 13 * * *',
    onTick: function () {
      // Runs everyday
      // at 00:15:13 PM

    },
    start: false,
  });
  job.start();
});



/**
 * Process for get new release products
 */
jobs.process('getNewReleases', function onProcessNewReleases(job, done) {
  helper.getNewReleases(job.data.url, function onComplete(err) {
    if (err) {
      console.log(err);
      return;
    };
    done();
  });
});

jobs.process('getProductDetail', function onProcessProductDetail(job, done) {
  var asin = job.data.asin;
  helper.getProductDetail(asin, function onDetail(err, aProduct) {
    if (err) {
      console.log(err);
      return;
    };

    Product.findOne({
      asin: asin
    }, function onFound(err, product) {
      if (err) {
        console.log(err);

        // Redo it again
        jobs.create('getProductDetail', {
          title: 'Get Product Detail on' + asin,
          asin: asin
        }).save(function onJobCreated(err) {
          if (err) {
            console.log(err);
            return;
          }
        });
        return;
      };

      product.title = aProduct.title;
      product.rank = aProduct.rank;
      product.description = aProduct.description;
      product.images = aProduct.images;
      product.releaseDate = aProduct.releaseDate;
      product.save(function onSaved(err) {
        if (err) {
          console.log(err);
          return;
        };
        console.log('Done detail for ' + asin);
        return done();
      });
    });
  });
});

jobs.promote();

jobs.process('getGoogleResults', function onPrecessGoogleResults(job, done) {
  var asin = job.data.asin;
  var keyword = job.data.keyword;
  var matching = job.data.matching;
  helper.getGoogleResults(job.data.keyword, function onComplete(err,
    results) {
    if (err) {
      console.log(err);
      return;
    };
    Product.findOne({
      asin: asin
    }, function onFound(err, product) {
      if (err) {
        console.log(err);
        return;
      };

      console.log(keyword + ' Google ' + matching + ' Results: ' +
        results);
      if ('phrase' === matching) {
        product.googlePhraseResults = parseInt(results);
      } else {
        product.googleResults = parseInt(results);
      };

      product.keyword = keyword;
      product.permalink = keyword.replace(/%22/g, '');
      product.permalink = product.permalink.replace(
        /[^a-zA-Z0-9 ]/g, '');
      product.permalink = product.permalink.replace(/ /g, '-');
      console.log(product.permalink);

      product.save(function onUpdateProductGoogelResults(err) {
        if (err) {
          console.log(err);
          return;
        };
        done();
      });
    });
  });
});


jobs.process('getGenKeywords', function (job, done) {
  var website = job.data.website;
  helper.getAmazonUrl(job.data.url, function (html) {
    // New API return keywords and next page url
    helper.getKeywordsAndNextPageFromHtml(html,
      function (keywords, nextPage) {

        for (var i = keywords.length - 1; i >= 0; i--) {
          //keywords[i]
          var newKeyword = new Keyword(keywords[i]);
          newKeyword.userId = website.userId;
          newKeyword.website = website._id;
          newKeyword.save();
        };

        // If next page is defined
        if (nextPage && nextPage !== '') {
          var nextPageUrl = 'http://www.amazon.com' + nextPage;
          jobs.create('getGenKeywords', {
            title: 'Generate keywords from ' + url,
            url: nextPageUrl,
            website: website
          }).save(function onJobCreated(err) {
            if (err) {
              console.log(err);
              return callback(err);
            };

            return done();
          });
        } else {
          return done();
        };
      }); // getKeywordAndNextPageFromHtml
  }); // getAmazonUrl
});

module.exports = router;
