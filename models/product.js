var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var productSchema = new Schema({
  rank: Number,
  asin: String,
  title:  String,
  keyword: String,
  permalink: String,
  platform: String,
  brand:   String,
  audienceRating: String,
  created: { type: Date, default: Date.now },
  updated: { type: Date, default: Date.now },
  releaseDate: Date,
  release: Boolean,
  imageUrl: String,
  images: [String],
  price: String,
  googleResults: Number,
  googlePhraseResults: Number
});

module.exports = mongoose.model('Product', productSchema);