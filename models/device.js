var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var deviceSchema = new Schema({
  regid: String,
  type: String,
  cat: String,
  created: {type: Date, default:Date.now},
});

module.exports = mongoose.model('Device', deviceSchema);