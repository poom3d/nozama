var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var templateSchema = new Schema({
  name: String,
  path: String,
  created: { type: Date, default: Date.now },
  updated: { type: Date, default: Date.now },
});

module.exports = mongoose.model('Template', templateSchema);