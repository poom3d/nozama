var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var prSchema = new Schema({
  url: String,
  pr: String,
});

module.exports = mongoose.model('Pr', prSchema);