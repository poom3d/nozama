var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var postSchema = new Schema({
  asin: String,
  title: String,
  content: String,
  created: { type: Date, default: Date.now },
  updated: { type: Date, default: Date.now },
  userId: { type: Schema.Types.ObjectId, ref: 'User' },
  status: { type: String, default:'pending' }
});

module.exports = mongoose.model('Post', postSchema);