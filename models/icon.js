var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var iconSchema = new Schema({
  name: String,
  url: String,
  attribution: String,
  created: { type: Date, default: Date.now },
  updated: { type: Date, default: Date.now },
});

module.exports = mongoose.model('Icon', iconSchema);