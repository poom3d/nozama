var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var networkSchema = new Schema({
  created: { type: Date, default: Date.now },
  updated: { type: Date, default: Date.now },
  url: String,
  username: { type: String, default: '' },
  password: { type: String, default: '' },
  userId: { type: Schema.Types.ObjectId, ref: 'User' },
});

module.exports = mongoose.model('Network', networkSchema);