var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var keywordSchema = new Schema({
  asin: String,
  keyword: String,
  title: String,
  audienceRating: String,
  created: { type: Date, default: Date.now },
  updated: { type: Date, default: Date.now },
  price: String,
  googleResults: Number,
  googlePhraseResults: Number,
  freeshipping: Boolean,
  fastshipping: Boolean,
  star: String,
  url: String,
  node: String,
  status: { type: String, default: 'published'},
  description: { type: String, default: '' },
  imageUrl: { type: String, default: '' },
  content: { type: String, default: '' },
  trackingId: { type: String, default: '' },
  permalink: { type: String, default: '' },
  userId: { type: Schema.Types.ObjectId, ref: 'User' },
  website: { type: Schema.Types.ObjectId, ref: 'Website' },
});

module.exports = mongoose.model('Keyword', keywordSchema);