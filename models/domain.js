var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var domainSchema = new Schema({
  pr: { type: Schema.Types.ObjectId, ref: 'Pr' },
  url: String,
  userId: { type: Schema.Types.ObjectId, ref: 'User' },
  created: { type: Date, default: Date.now },
  updated: { type: Date, default: Date.now },
});

module.exports = mongoose.model('Domain', domainSchema);