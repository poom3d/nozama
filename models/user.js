var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
  username: String,
  email: String,
  password: String,
  paymentId: {type: String, default:''},
  status: {type: String, default:'pending'},
  package: {type: String, default: 'standard'},
  term: {type: String, default: 'monthly'},
  dueDate: {type: Date, default:Date.now},
  trackingId: {type: String, default:''},
});

module.exports = mongoose.model('User', userSchema);