var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var articleSchema = new Schema({
  title: String,
  permalink: String,
  byline: String,
  summary: String,
  content: String,
  resource: String,
  source: String,
  related: [String],
  cat: String,
  created: { type: Date, default: Date.now },
  updated: { type: Date, default: Date.now },
  eid: String
});

module.exports = mongoose.model('Article', articleSchema);