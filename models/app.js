var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var appSchema = new Schema({
  name: String,
  summary: String,
  ios: String,
  android: String,
  cat: String,
  created: { type: Date, default: Date.now },
  updated: { type: Date, default: Date.now },
  eid: String,
  userId: { type: Schema.Types.ObjectId, ref: 'User' },
});

module.exports = mongoose.model('App', appSchema);