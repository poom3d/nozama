var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var keywordCacheSchema = new Schema({
  asin: String,
  keyword: String,
  title: String,
  audienceRating: String,
  created: { type: Date, default: Date.now },
  updated: { type: Date, default: Date.now },
  price: String,
  googleResults: Number,
  googlePhraseResults: Number,
  freeshipping: Boolean,
  fastshipping: Boolean,
  star: String,
  url: String,
  node: String,
  imageUrl: { type: String, default: '' },
  permalink: { type: String, default: '' },
});

module.exports = mongoose.model('KeywordCache', keywordCacheSchema);