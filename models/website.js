var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var websiteSchema = new Schema({
  created: { type: Date, default: Date.now },
  updated: { type: Date, default: Date.now },
  url: String,
  title: { type: String, default: '' },
  description: { type: String, default: '' },
  trackingId: { type: String, default: '' },
  logo: { type: Schema.Types.ObjectId, ref: 'Icon' },
  userId: { type: Schema.Types.ObjectId, ref: 'User' },
});

module.exports = mongoose.model('Website', websiteSchema);