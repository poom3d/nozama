var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var kue = require('kue');
var session = require('express-session');
var RedisStore = require('connect-redis')(session);

var fs = require('fs');
var routes = require('./routes/index');
var crawler = require('./routes/crawler');
var bot = require('./routes/bot');
var api = require('./routes/api');

var app = express();

var env = process.env.NODE_ENV || 'development';



try {
  if ('development' === env) {
    var configJSON = fs.readFileSync(__dirname + "/config.json");
  } else {
    console.log('here?')
    var configJSON = fs.readFileSync(__dirname + "/config_prod.json");
  }

  var config = JSON.parse(configJSON.toString());

} catch (e) {
  console.error("File config.json not found or is invalid: " + e.message);
  process.exit(1);
}
routes.init(config);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(cookieParser('iris'));
app.use(session({secret: 'iris',
                saveUninitialized: true,
                store: new RedisStore(),
                resave: true}));
app.use(express.static(path.join(__dirname, 'public')));

// Last modified bug
// app.get('/api/*', function(req, res, next){
//   res.setHeader('Last-Modified', (new Date()).toUTCString());
//   next();
// });
app.use('/api', api);
app.use('/bot', bot);
app.use('/', routes);



app.use('/crawl', crawler);

app.use('/bot', bot);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if ('development' === env) {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });

  mongoose.connect('mongodb://localhost:27017/preordered');
} else if ('production' === env) {
  mongoose.connect('mongodb://localhost:27017/preordered');
};

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

var debug = require('debug')('preordered');

app.set('port', process.env.PORT || 3000);

var server = app.listen(app.get('port'), function() {
  console.log('Express server listening on port ' + server.address().port);
});

kue.app.listen(3001);

// Create admin user if not exist
var User = require('./models/user');
User.findOne({username: 'admin'}, function(err, user) {
  if (err) {
    console.log('Something Wrong!');
    return;
  };
  if (!user) {
    console.log('create admin');
    var adminUser = new User();
    adminUser.username = 'admin';
    adminUser.password = '********';
    adminUser.status = 'admin';
    adminUser.email = 'poom3d@gmail.com';
    adminUser.save();
  };
});